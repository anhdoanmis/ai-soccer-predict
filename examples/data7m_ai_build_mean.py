import kickscore as ks

from datetime import datetime
from dateutil.parser import parse
from scipy.signal import find_peaks
import sqlite3
from sqlite3 import Error
import numpy as np
import matplotlib.pyplot as plt
import itertools
import json


rows = []
items = []
cutoff = datetime(2020, 12, 1).timestamp()
conn = None


def print_plot_scores(model, item_name, resolution=None, timestamps=False):
    # colors = itertools.cycle(plt.cm.tab10(np.linspace(0, 1, 10)))
    if resolution is None:
        first = min(obs.t for obs in model.observations)
        last = max(obs.t for obs in model.observations)
        resolution = 100 / (last - first)
    # fig, ax = plt.subplots(figsize=figsize)
    data_graph = []
    ms_peaks = np.empty
    ms_troughs = np.empty
    # data_graph_index = 0
    
        # color = next(colors)
    ts, _, _ = model.item[item_name].scores
    try:
        first = min(ts)
        last = max(ts)
        ts = np.linspace(first, last, num=int(resolution * (last - first)))
        ms, vs = model.item[item_name].predict(ts)
        # std = np.sqrt(vs)
        if timestamps:
            ts = [datetime.fromtimestamp(t) for t in ts]
        # print(ts)
        # print(type(ts))
        # print(ms)
        # print(type(ms))
        ms_peaks, _ = find_peaks(ms) 
        ms_troughs, _= find_peaks(-ms)
        for i in range(len(ts)):
            # print(ts[i])
            # print(type(ts[i]))
            data_graph.append({'Team': item_name, 'Date': str(ts[i]), 'Mean': ms[i]})
            # data_graph_index += 1
    except:
        print('Error ts ', item_name)        
        pass
    return data_graph, ms_peaks, ms_troughs
    # ax.plot(ts, ms, color=color, label=name)
    # print(ax.plot(ts, ms, color=color, label=name))
    # ax.fill_between(ts, ms-std, ms+std, color=color, alpha=0.1)
    # for spine in ("top", "right", "bottom", "left"):
    # ax.spines[spine].set_visible(False)
    # ax.grid(axis="x", alpha=0.5)
    # ax.legend()


def fit_predict_home_away(team_ID):
    #team_ID = 1
    #team_name = 'Djurgardens'
    #teams = set()
    teams_7m = set()
    #teams_7m_noh = set()
    #observations = list()
    observations_7m = list()
    observations_7m_noh = list()
    #rows = []
    rows_7m = []
    cutoff = datetime(2020, 12, 1).timestamp()
    conn = None
    # try:
    #     conn = sqlite3.connect('database.sqlite')
    #     cur = conn.cursor()
    #     cur.execute("SELECT Datetime, HomeTeam, AwayTeam, FTHG, FTAG FROM football_data Where HomeTeam = 'Liverpool' or AwayTeam = 'Liverpool' or HomeTeam = 'Chelsea' or AwayTeam = 'Chelsea' order by Datetime ASC")

    #     rows = cur.fetchall()
    # except Error as e:
    #     print(e)
    
    conn = sqlite3.connect('data7m.db')

    cur = conn.cursor()
    cur.execute(f"SELECT DISTINCT Div FROM soccer_data Where (HomeID = '{team_ID}' or AwayID = '{team_ID}')")

    rows_div = cur.fetchall()

    for row_div in rows_div:

        cur = conn.cursor()
        cur.execute(f"SELECT Div,Time_, Home, Away, FTHG, FTAG, HTHG, HTAG, Statement, HomeID, AwayID FROM soccer_data Where (HomeID = '{team_ID}' or AwayID = '{team_ID}') AND Div = '{row_div[0]}' order by Time_ ASC")

        rows_7m = cur.fetchall()
        
        if len(rows_7m) > 50:
            for row in rows_7m:
                t_date = datetime.strptime(row[1], "%Y-%m-%d %H:%M:%S")
                if t_date < parse('01/03/1970'):
                    t = - (parse('01/03/1970') - t_date).days * 86400 + 25200 + parse(
                        '01/03/1970').timestamp()
                else:
                    t = t_date.timestamp()
                #t = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S").timestamp()
                if t > cutoff:
                    break
                teams_7m.add(str(row[9]))
                teams_7m.add(str(row[10]))
                #teams_7m_noh.add(row[9])
                #teams_7m_noh.add(row[10])
                if row[8] == "Win" or row[8] == "Win1/2":
                    observations_7m.append({
                        "winners": [str(row[9])],
                        "losers": [str(row[10])],
                        "t": t,
                    })
                if row[8] == "Loss" or row[8] == "Loss1/2":
                    observations_7m.append({
                        "winners": [str(row[10])],
                        "losers": [str(row[9])],
                        "t": t,
                    })
                if int(row[4]) > int(row[5]):
                    observations_7m_noh.append({
                        "winners": [str(row[9])],
                        "losers": [str(row[10])],
                        "t": t,
                    })
                if int(row[4]) < int(row[5]):
                    observations_7m_noh.append({
                        "winners": [str(row[10])],
                        "losers": [str(row[9])],
                        "t": t,
                    })
            # It is a bit more convenient to specify lengthscales in yearly units.
            seconds_in_year = 365.25 * 24 * 60 * 60

            # model = ks.BinaryModel()
            model_7m = ks.BinaryModel()
            model_7m_noh = ks.BinaryModel()
            kernel = (ks.kernel.Constant(var=0.03)
                    + ks.kernel.Matern32(var=0.138, lscale=1.753*seconds_in_year))
            # for team in teams:
            #     model.add_item(team, kernel=kernel)

            # for obs in observations:
            #     model.observe(**obs)

            for team in teams_7m:
                model_7m.add_item(team, kernel=kernel)
                model_7m_noh.add_item(team, kernel=kernel)

            for obs in observations_7m:
                model_7m.observe(**obs)   

            for obs in observations_7m_noh:
                model_7m_noh.observe(**obs)

            # converged = model.fit()
            # if converged:
            #     print("Model has converged.")
            converged_7m = model_7m.fit()
            # if converged_7m:
            #     print("Model_7m has converged.")
            converged_7m_noh = model_7m_noh.fit()
            # if converged_7m_noh:
            #     print("Model_7m_noh has converged.")
            
            graph_7m, ms_peaks_7m, ms_troughs_7m = print_plot_scores(model_7m, str(team_ID), resolution=10/seconds_in_year, timestamps=True)        
            graph_7m_noh, ms_peaks_7m_noh, ms_troughs_7m_noh =  print_plot_scores(model_7m_noh, str(team_ID), resolution=10/seconds_in_year, timestamps=True)  
            
            try:
                sql = ''' UPDATE team
                                SET GP_1x2 = ?,
                                GP_AH = ?,
                                GP_1x2_local_mm = ?,
                                GP_AH_local_mm = ?                               
                                WHERE ID = ?
                                '''
                #print(graph_7m)
                cur.execute(sql, (json.dumps(graph_7m_noh), json.dumps(graph_7m), 
                json.dumps({"peaks": json.dumps(ms_peaks_7m_noh.tolist()), "troughs": json.dumps(ms_troughs_7m_noh.tolist())}), 
                json.dumps({"peaks": json.dumps(ms_peaks_7m.tolist()), "troughs": json.dumps(ms_troughs_7m.tolist())}), 
                team_ID)
                )
                conn.commit()
            except:
                print("Error update ", team_ID)
                pass
                
                
        

"""fig, ax = model.plot_scores(
        #items=["LAL", "CHI", "BOS"],
        items=["Liverpool"],
        resolution=10/seconds_in_year,
        figsize=(14.0, 3.0),
        timestamps=True)
ax.set_title("World Soccer");"""

# data = print_plot_scores(model=model, items=items, resolution=10/seconds_in_year, figsize=(14.0, 3.0), timestamps=True)
# for team_ID in range(595053, 634010):
#     try: 
#         fit_predict_home_away(team_ID)
#         print("Done!", team_ID)
#     except Error as e:
#         print("Error!", team_ID)
#         print(e)
# p_win, _ = model.probabilities(["Club Leon"], ["Monterrey"], t=datetime.strptime("2020-08-03 03:00:00", "%Y-%m-%d %H:%M:%S").timestamp())
# print(p_win)
# with open('data.json', 'w') as f:
#    json.dump(data, f)
