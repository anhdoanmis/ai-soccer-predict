import sqlite3
from sqlite3 import Error
from datetime import datetime
conn = sqlite3.connect('data7m.db')
cur = conn.cursor()
training_start = datetime.strptime("2016-09-25 00:00:00", "%Y-%m-%d %H:%M:%S")
training_end = datetime.strptime("2020-11-20 00:00:00", "%Y-%m-%d %H:%M:%S")
cur.execute("""select Div, Time_, Home, Away, FTHG, FTAG, HTHG, HTAG, Statement,HomeSup,AwaySup,P_HH,HomeHandicap,Home_Avg_Goals_Last_5,Away_Avg_Goals_Last_5 FROM soccer_data 
                        WHERE HomeHandicap != "" AND Time_>='{start}' AND Time_<='{end}'
                        order by Time_ ASC""".format(start=training_start.strftime("%Y-%m-%d %H:%M:%S"),
                                                     end=training_end.strftime("%Y-%m-%d %H:%M:%S")))
rows = cur.fetchall()
for i in range(70000, len(rows)):
    count_home_goal_win_last_5, count_home_goal_all_last_5, count_away_goal_win_last_5, count_away_goal_all_last_5 = 0.0, 0.0, 0.0, 0.0
    home_last_5, away_last_5 = "", ""
    row = rows[i]
    
    for j in range(i - 1, 0, -1):        
        if str(rows[j][2]) == str(row[2]):            
            count_home_goal_win_last_5 += float(rows[j][4])
            count_home_goal_all_last_5 += 1.0
            home_last_5 = str(rows[j][12]) + ":" + str(rows[j][11]) + ":" + str(rows[j][13]) + ";" + home_last_5
            if count_home_goal_all_last_5 >= 5: break
        if str(rows[j][3]) == str(row[2]):
            count_home_goal_win_last_5 += float(rows[j][5])
            count_home_goal_all_last_5 += 1.0  
            P_away = 1 - float(rows[j][11]) if rows[j][11] is not None else 0
            home_last_5 = "-" + str(rows[j][12]) + ":" + str(P_away) + ":" + str(rows[j][14]) + ";" + home_last_5
            if count_home_goal_all_last_5 >= 5: break
    for j in range(i - 1, 0, -1):
        if str(rows[j][2]) == str(row[3]):
            count_away_goal_win_last_5 += float(rows[j][4])
            count_away_goal_all_last_5 += 1.0
            away_last_5 = str(rows[j][12]) + ":" + str(rows[j][11]) + ":" + str(rows[j][13]) + ";" + away_last_5
            if count_away_goal_all_last_5 >= 5: break
        if str(rows[j][3]) == str(row[3]):
            count_away_goal_win_last_5 += float(rows[j][5])
            count_away_goal_all_last_5 += 1.0  
            P_away = 1 - float(rows[j][11]) if rows[j][11] is not None else 0            
            away_last_5 = "-" + str(rows[j][12]) + ":" + str(P_away) + ":" + str(rows[j][14]) + ";" + away_last_5
            if count_away_goal_all_last_5 >= 5: break

    home_avg_goals_last_5 = count_home_goal_win_last_5/count_home_goal_all_last_5 if count_home_goal_all_last_5 > 0 else 100.0    
    away_avg_goals_last_5 = count_away_goal_win_last_5/count_away_goal_all_last_5 if count_away_goal_all_last_5 > 0 else 100.0
    home_last_5 = home_last_5.replace("--", "") if count_home_goal_all_last_5 > 0 else ""
    away_last_5 = away_last_5.replace("--", "") if count_away_goal_all_last_5 > 0 else ""
    
    #try:        
    sql = ''' UPDATE soccer_data
                    SET Home_Avg_Goals_Last_5 = ?,                        
                    Away_Avg_Goals_Last_5 = ?
                                                                            
                    WHERE Time_ = ?
                    AND Home = ?
                    AND Away = ?
                    '''
    
    cur.execute(sql, (home_avg_goals_last_5, away_avg_goals_last_5, row[1], row[2], row[3]))
    conn.commit()
    print(row[1],";",row[2],";",row[3])
    #except:
    #    pass