import sqlite3
from sqlite3 import Error
import bs4
import kickscore as ks

from datetime import datetime
from dateutil.parser import parse

import numpy as np
import matplotlib.pyplot as plt
import itertools
import json

teams = set()
observations = list()
rows = []
items = []
cutoff = datetime(2020, 12, 1).timestamp()
conn = None
try:
    conn = sqlite3.connect('database.sqlite')
    cur = conn.cursor()
    cur.execute("SELECT Datetime, HomeTeam, AwayTeam, FTHG, FTAG FROM football_data order by Datetime ASC")
    # cur.execute("SELECT Datetime, HomeTeam, AwayTeam, FTHG, FTAG FROM football_data Where HomeTeam='Liverpool' or AwayTeam='Liverpool' order by Datetime ASC")
    rows = cur.fetchall()
    cur.execute("select HomeTeam from football_data UNION select AwayTeam from football_data")

    items_list = cur.fetchall()
    items = [i[0] for i in items_list]
    # items = ['Liverpool']
    # print(items)
except Error as e:
    print(e)

for row in rows:
    t_date = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S")
    if datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S") < parse('01/03/1970'):
        t = - (parse('01/03/1970') - datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S")).days * 86400 + 25200 + parse(
            '01/03/1970').timestamp()
    else:
        t = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S").timestamp()
    # t = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S").timestamp()
    if t > cutoff:
        break
    teams.add(row[1])
    teams.add(row[2])
    if int(row[3]) > int(row[4]):
        observations.append({
            "winners": [row[1]],
            "losers": [row[2]],
            "t": t,
        })
    else:
        observations.append({
            "winners": [row[2]],
            "losers": [row[1]],
            "t": t,
        })
# It is a bit more convenient to specify lengthscales in yearly units.
seconds_in_year = 365.25 * 24 * 60 * 60

model = ks.BinaryModel()
kernel = (ks.kernel.Constant(var=0.03)
          + ks.kernel.Matern32(var=0.138, lscale=1.753 * seconds_in_year))
for team in teams:
    model.add_item(team, kernel=kernel)

for obs in observations:
    model.observe(**obs)

converged = model.fit()
if converged:
    print("Model has converged.")

file = open("data_enetscores.txt", "r")
html = file.read()
soup = bs4.BeautifulSoup(html, "html.parser")
soup = soup.find_all('div', class_="wff_soccer_event_row")

matches = []
for item in soup:
    time = item.find('div', class_="wff_event_start_time").find('span').text
    team = item.find_all('div', class_="wff_event_participant_name")
    odd = item.find_all('div', class_="wff_soccer_odds_s")
    AvgH = 0.0
    AvgD = 0.0
    AvgA = 0.0
    if len(odd) == 3:
        AvgH = odd[0].find("span").text
        AvgD = odd[1].find("span").text
        AvgA = odd[2].find("span").text
    matches.append({"Time": time, "HomeTeam": team[0].text, "AwayTeam": team[1].text,
                    "AvgH": AvgH, "AvgD": AvgD, "AvgA": AvgA, "predictH": 0, "select": 0})

for item in matches:
    avgh = 0.0
    try:
        avgh = float(item['AvgH'])
    except:
        pass

    if 2.2 <= avgh <= 2.7 or 1.6 <= avgh < 2:
        p_win = 0.0
        home = item["HomeTeam"].strip()
        away = item["AwayTeam"].strip()
        """count_match = 0
        try:
            conn = sqlite3.connect('database.sqlite')
            cur = conn.cursor()
            cur.execute(
                "select count(*) from football_data where HomeTeam = '{home}' or AwayTeam = '{home}'".format(home=home))
            count_match = cur.fetchone()[0]
            if 300 <= count_match:
                cur.execute(
                    "select count(*) from football_data where HomeTeam = '{away}' or AwayTeam = '{away}'".format(
                        away=away))
                count_match = cur.fetchone()[0]

        except:
            pass
        """
        # print(count_match)
        # if 300 <= count_match:
        try:
            # print(item["HomeTeam"], " - ", item["AwayTeam"], " - Result: ", item["Result"])

            p_win, _ = model.probabilities([home], [away],
                                           t=datetime.now().timestamp())
            item["predictH"] = p_win
            if 2.2 <= avgh <= 2.7 and 0.4294376499106303 <= p_win <= 0.7548212743484494:
                item["select"] = 1
                print(home, " - ", away, ": Pick Home 0 P=", p_win)
            if 1.6 <= avgh < 1.7 and 0.6203990423734786 <= p_win <= 0.7677839457296286:
                item["select"] = 1
                print(home, " - ", away, ": Pick Home -0.75 P=", p_win)
            if 1.7 <= avgh < 2 and 0.6423996977595771 <= p_win <= 0.8240205818558227:
                item["select"] = 1
                print(home, " - ", away, ": Pick Home -0.5 P=", p_win)
            if 1.7 <= avgh < 2 and 0.23206961762242148 <= p_win <= 0.4616289290017005:
                item["select"] = 1
                p_away = 1 - p_win
                print(home, " - ", away, ": Pick Away +0.5 P=", p_away)

        except:
            pass
