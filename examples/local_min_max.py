import json
import sqlite3
from sqlite3 import Error
from datetime import datetime

items = []
try:
    conn = sqlite3.connect('database.sqlite')
    cur = conn.cursor()

    cur.execute("select HomeTeam from football_data UNION select AwayTeam from football_data")

    items_list = cur.fetchall()
    items = [i[0] for i in items_list]
    # items = ['Liverpool']
    # print(items)
except Error as e:
    print(e)
with open('data.json', 'r') as f:
    data_json = json.load(f)
strong_teams = []
weak_teams = []
for item in items:
    # if item == 'Liverpool':
    means = []
    local_maximum = []
    local_minimum = []
    for row in data_json:
        if row['Team'] == item:
            means.append([row['Date'], row['Mean']])
    for i in range(len(means)):
        if 2 < i < len(means) - 2:
            i_pre = i - 1
            is_down_trend = 0
            is_up_trend = 0
            is_down_or_up = 0

            while i_pre > 0 and is_up_trend < 2 and is_down_trend < 2:
                if means[i_pre][1] > means[i_pre + 1][1]: is_down_trend += 1
                if means[i_pre][1] < means[i_pre + 1][1]: is_up_trend += 1
                i_pre -= 1
            # go up to i
            if is_up_trend == 2 and is_down_trend == 0: is_down_or_up = 1
            # go down to i
            if is_up_trend == 0 and is_down_trend == 2: is_down_or_up = 2
            is_down_trend = 0
            is_up_trend = 0
            i_next = i + 1

            while i_next < len(means) and is_up_trend < 2 and is_down_trend < 2:
                if means[i_next][1] > means[i_next - 1][1]: is_up_trend += 1
                if means[i_next][1] < means[i_next - 1][1]: is_down_trend += 1
                i_next += 1
                # go up to i
            if is_down_or_up == 1 and is_up_trend == 0 \
                    and (is_down_trend == 2 or (is_down_trend == 1 and i_next == len(means))):
                local_maximum.append([means[i][0], means[i][1]])
            # go down to i
            if is_down_or_up == 2 and is_down_trend == 0 \
                    and (is_up_trend == 2 or (is_up_trend == 1 and i_next == len(means))):
                local_minimum.append([means[i][0], means[i][1]])
    try:
        local_maximum.append([means[len(means) - 1][0], means[len(means) - 1][1]])
    except:
        pass
    try:
        local_minimum.append([means[len(means) - 1][0], means[len(means) - 1][1]])
    except: pass
    max_length_strength_all = -9999999.99
    max_length_strength_half = -9999999.99
    year_n = 2013
    for i in range(len(local_minimum), 0):
        if int(datetime.strptime(local_minimum[i][0], "%Y-%m-%d %H:%M:%S").strftime("%Y")) >= year_n:
            max_length_strength_half = means[len(means) - 1][1] - local_minimum[i][1]
    if max_length_strength_half == max_length_strength_all:
        for i in range(len(means), 0):
            if int(datetime.strptime(means[i][0], "%Y-%m-%d %H:%M:%S").strftime("%Y")) >= year_n:
                max_length_strength_half = means[len(means) - 1][1] - means[i][1]
    for i in range(len(local_minimum)):
        for j in range(len(local_maximum)):
            t_local_minimum = datetime.now()
            t_local_maximum = datetime.now()
            try:
                t_local_minimum = datetime.strptime(local_minimum[i][0], "%Y-%m-%d %H:%M:%S.%f")
            except:
                pass
            try:
                t_local_minimum = datetime.strptime(local_minimum[i][0], "%Y-%m-%d %H:%M:%S")
            except:
                pass
            try:
                t_local_maximum = datetime.strptime(local_maximum[j][0], "%Y-%m-%d %H:%M:%S.%f")
            except:
                pass
            try:
                t_local_maximum = datetime.strptime(local_maximum[j][0], "%Y-%m-%d %H:%M:%S")
            except:
                pass
            if t_local_minimum.timestamp() < t_local_maximum.timestamp():
                if float(local_maximum[j][1]) - float(local_minimum[i][1]) > max_length_strength_all:
                    max_length_strength_all = float(local_maximum[j][1]) - float(local_minimum[i][1])
            if int(t_local_maximum.strftime("%Y")) >= year_n \
                    and int(t_local_minimum.strftime("%Y")) >= year_n \
                    and t_local_minimum.timestamp() < t_local_maximum.timestamp():
                if float(local_maximum[j][1]) - float(local_minimum[i][1]) > max_length_strength_half:
                    max_length_strength_half = float(local_maximum[j][1]) - float(local_minimum[i][1])
    if max_length_strength_half > 0.5:
        strong_teams.append([item, max_length_strength_all, max_length_strength_half])
    if 0.2 < max_length_strength_all < 0.5:
        weak_teams.append([item, max_length_strength_all, max_length_strength_half])

separator = '","'
strong_teams_name = [i[0] for i in strong_teams]
weak_teams_name = [i[0] for i in weak_teams]
#print(strong_teams_name)
#print(separator.join(strong_teams_name))
sql = """select Datetime, HomeTeam, AwayTeam, FTHG,FTAG, AvgH, AvgA, AvgA/(AvgA + AvgH) as odd, PredictH,
                CASE
	WHEN AvgH <= 2.7 AND AvgH >=2.2 THEN 0
	WHEN AvgH < 2.2 AND AvgH >= 2 THEN -0.25
	WHEN AvgH < 2 AND AvgH >= 1.7 THEN -0.5
	WHEN AvgH < 1.7 AND AvgH >= 1.6 THEN -0.75
	WHEN AvgH < 1.6 AND AvgH >= 1.5 THEN -1
	WHEN AvgH < 1.5 AND AvgH >= 1.4 THEN -1.25
	WHEN AvgH < 1.4 AND AvgH >= 1.3 THEN -1.5
	WHEN AvgH < 1.3 AND AvgH >= 1.2 THEN -1.75
	WHEN AvgH < 1.2 AND AvgH >= 1 THEN -2

	WHEN AvgA <= 2.7 AND AvgA >=2.2 THEN 0
	WHEN AvgA < 2.2 AND AvgA >= 2 THEN 0.25
	WHEN AvgA < 2 AND AvgA >= 1.7 THEN 0.5
	WHEN AvgA < 1.7 AND AvgA >= 1.6 THEN 0.75
	WHEN AvgA < 1.6 AND AvgA >= 1.5 THEN 1
	WHEN AvgA < 1.5 AND AvgA >= 1.4 THEN 1.25
	WHEN AvgA < 1.4 AND AvgA >= 1.3 THEN 1.5
	WHEN AvgA < 1.3 AND AvgA >= 1.2 THEN 1.75
	WHEN AvgA < 1.2 AND AvgA >= 1 THEN 2
	WHEN AvgH is NULL THEN NULL
	WHEN AvgA is NULL THEN NULL
	ELSE 0
	END AS HH,

CASE
	WHEN AvgH <= 2.7 AND AvgH >=2.2 AND FTHG >= FTAG THEN 1
	WHEN AvgH < 2.2 AND AvgH >= 2 AND FTHG-0.25 > FTAG THEN 1
	WHEN AvgH < 2 AND AvgH >= 1.7 AND FTHG -0.5 > FTAG THEN 1
	WHEN AvgH < 1.7 AND AvgH >= 1.6 AND FTHG-0.75 > FTAG THEN 1
	WHEN AvgH < 1.6 AND AvgH >= 1.5 AND FTHG-1 > FTAG THEN 1
	WHEN AvgH < 1.5 AND AvgH >= 1.4 AND FTHG-1.25 > FTAG THEN 1
	WHEN AvgH < 1.4 AND AvgH >= 1.3 AND FTHG-1.5 > FTAG THEN 1
	WHEN AvgH < 1.3 AND AvgH >= 1.2 AND FTHG-1.75 > FTAG THEN 1
	WHEN AvgH < 1.2 AND AvgH >= 1 AND FTHG-2 > FTAG THEN 1

	WHEN AvgA <= 2.7 AND AvgA >=2.2 AND FTHG >= FTAG THEN 1
	WHEN AvgA < 2.2 AND AvgA >= 2 AND FTHG+0.25 > FTAG THEN 1
	WHEN AvgA < 2 AND AvgA >= 1.7 AND FTHG+0.5 > FTAG THEN 1
	WHEN AvgA < 1.7 AND AvgA >= 1.6 AND FTHG+0.75 > FTAG THEN 1
	WHEN AvgA < 1.6 AND AvgA >= 1.5 AND FTHG+1 > FTAG THEN 1
	WHEN AvgA < 1.5 AND AvgA >= 1.4 AND FTHG+1.25 > FTAG THEN 1
	WHEN AvgA < 1.4 AND AvgA >= 1.3 AND FTHG+1.5 > FTAG THEN 1
	WHEN AvgA < 1.3 AND AvgA >= 1.2 AND FTHG+1.75 > FTAG THEN 1
	WHEN AvgA < 1.2 AND AvgA >= 1 AND FTHG+2 > FTAG THEN 1
	WHEN AvgH is NULL THEN NULL
	WHEN AvgA is NULL THEN NULL
	ELSE 0
	END AS HW 
                FROM football_data where CAST(strftime('%sY',Datetime) AS INTEGER)>=2018
                AND 
                (HomeTeam in ("{strong}") AND AwayTeam in("{weak}"))
                """.format(strong = separator.join(strong_teams_name), weak = separator.join(weak_teams_name))
sql2 = """select Datetime, HomeTeam, AwayTeam, FTHG,FTAG, AvgH, AvgA, AvgA/(AvgA + AvgH) as odd, PredictH,
                CASE
	WHEN AvgH <= 2.7 AND AvgH >=2.2 THEN 0
	WHEN AvgH < 2.2 AND AvgH >= 2 THEN -0.25
	WHEN AvgH < 2 AND AvgH >= 1.7 THEN -0.5
	WHEN AvgH < 1.7 AND AvgH >= 1.6 THEN -0.75
	WHEN AvgH < 1.6 AND AvgH >= 1.5 THEN -1
	WHEN AvgH < 1.5 AND AvgH >= 1.4 THEN -1.25
	WHEN AvgH < 1.4 AND AvgH >= 1.3 THEN -1.5
	WHEN AvgH < 1.3 AND AvgH >= 1.2 THEN -1.75
	WHEN AvgH < 1.2 AND AvgH >= 1 THEN -2

	WHEN AvgA <= 2.7 AND AvgA >=2.2 THEN 0
	WHEN AvgA < 2.2 AND AvgA >= 2 THEN 0.25
	WHEN AvgA < 2 AND AvgA >= 1.7 THEN 0.5
	WHEN AvgA < 1.7 AND AvgA >= 1.6 THEN 0.75
	WHEN AvgA < 1.6 AND AvgA >= 1.5 THEN 1
	WHEN AvgA < 1.5 AND AvgA >= 1.4 THEN 1.25
	WHEN AvgA < 1.4 AND AvgA >= 1.3 THEN 1.5
	WHEN AvgA < 1.3 AND AvgA >= 1.2 THEN 1.75
	WHEN AvgA < 1.2 AND AvgA >= 1 THEN 2
	WHEN AvgH is NULL THEN NULL
	WHEN AvgA is NULL THEN NULL
	ELSE 0
	END AS HH,

CASE
	WHEN AvgH <= 2.7 AND AvgH >=2.2 AND FTHG >= FTAG THEN 1
	WHEN AvgH < 2.2 AND AvgH >= 2 AND FTHG-0.25 > FTAG THEN 1
	WHEN AvgH < 2 AND AvgH >= 1.7 AND FTHG -0.5 > FTAG THEN 1
	WHEN AvgH < 1.7 AND AvgH >= 1.6 AND FTHG-0.75 > FTAG THEN 1
	WHEN AvgH < 1.6 AND AvgH >= 1.5 AND FTHG-1 > FTAG THEN 1
	WHEN AvgH < 1.5 AND AvgH >= 1.4 AND FTHG-1.25 > FTAG THEN 1
	WHEN AvgH < 1.4 AND AvgH >= 1.3 AND FTHG-1.5 > FTAG THEN 1
	WHEN AvgH < 1.3 AND AvgH >= 1.2 AND FTHG-1.75 > FTAG THEN 1
	WHEN AvgH < 1.2 AND AvgH >= 1 AND FTHG-2 > FTAG THEN 1

	WHEN AvgA <= 2.7 AND AvgA >=2.2 AND FTHG >= FTAG THEN 1
	WHEN AvgA < 2.2 AND AvgA >= 2 AND FTHG+0.25 > FTAG THEN 1
	WHEN AvgA < 2 AND AvgA >= 1.7 AND FTHG+0.5 > FTAG THEN 1
	WHEN AvgA < 1.7 AND AvgA >= 1.6 AND FTHG+0.75 > FTAG THEN 1
	WHEN AvgA < 1.6 AND AvgA >= 1.5 AND FTHG+1 > FTAG THEN 1
	WHEN AvgA < 1.5 AND AvgA >= 1.4 AND FTHG+1.25 > FTAG THEN 1
	WHEN AvgA < 1.4 AND AvgA >= 1.3 AND FTHG+1.5 > FTAG THEN 1
	WHEN AvgA < 1.3 AND AvgA >= 1.2 AND FTHG+1.75 > FTAG THEN 1
	WHEN AvgA < 1.2 AND AvgA >= 1 AND FTHG+2 > FTAG THEN 1
	WHEN AvgH is NULL THEN NULL
	WHEN AvgA is NULL THEN NULL
	ELSE 0
	END AS HW 
                FROM football_data where CAST(strftime('%sY',Datetime) AS INTEGER)>=2018
                AND 
                (HomeTeam in ("{weak}") AND AwayTeam in("{strong}"))
                """.format(strong = separator.join(strong_teams_name), weak = separator.join(weak_teams_name))
f = open("sql.txt", "w")
f.write(sql)
f.write("\n")
f.write("\n")
f.write(sql2)
f.close()
# print(sql)

try:
    conn = sqlite3.connect('database.sqlite')
    cur = conn.cursor()

    cur.execute(sql)

    items_list = cur.fetchall()
    count_win = 0
    count_item = 0
    for i in items_list:
        if i[10] == 0:
            count_item += 1
        if i[10] == 1:
            count_item += 1
            count_win += 1
    # items = ['Liverpool']
    print(count_win, '/', count_item, ' = ', float(count_win/count_item))

    cur.execute(sql2)

    items_list = cur.fetchall()
    count_win2 = 0
    count_item2 = 0
    for i in items_list:
        if i[10] == 0:
            count_item2 += 1
            count_win2 += 1
        if i[10] == 1:
            count_item2 += 1

    # items = ['Liverpool']
    print(count_win2, '/', count_item2, ' = ', float(count_win2 / count_item2))
except Error as e:
    print(e)

