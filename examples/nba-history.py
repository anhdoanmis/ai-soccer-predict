from datetime import datetime

cutoff = datetime(2019, 6, 1).timestamp()

dt = "1946-11-01"

date_object = datetime.strptime(dt, "%Y-%m-%d")

print("date_object =", date_object.timestamp())
