import sqlite3
from sqlite3 import Error

import bs4
import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
import pymysql.cursors
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='',                             
                             db='db_7m',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

conn = sqlite3.connect('data7m.db')
# cur = conn.cursor()
# cur.execute("SELECT MAX(Time_) FROM soccer_data")
# item = cur.fetchone()
cursor_1 = connection.cursor()
cursor_1.execute("SELECT MAX(Time_) AS Time_ FROM soccer_data")
item = cursor_1.fetchone()
date_from = datetime.datetime.strptime(str(item['Time_']), "%Y-%m-%d %H:%M:%S")
#date_to = datetime.datetime.strptime("2020-09-16 00:00:00", "%Y-%m-%d %H:%M:%S")
date_to = datetime.datetime.now() - datetime.timedelta(days=1) + datetime.timedelta(hours=4)
browser = webdriver.PhantomJS(executable_path=r'C:\phantomjs-2.1.1-windows\bin\phantomjs.exe')
while date_from <= date_to:
    url = "http://data.7msport.com/result/default_en.shtml?date={date}".format(date=date_from.strftime("%Y-%m-%d"))

    date = date_from.strftime("%Y-%m-%d")
    print(date)
    browser.get(url)

    try:
        element = browser.find_element(By.ID, "result_tb")
    except:
        element = None
        print("Blank!")
        date_from = date_from + datetime.timedelta(days=1)
    if element is not None:
        # print(element.get_attribute('innerHTML'))

        date_from = date_from + datetime.timedelta(days=1)
        soup = bs4.BeautifulSoup(element.get_attribute('innerHTML'), "html.parser")
        # print(soup.text)
        tr_tags = soup.find_all('tr')
        # print(tr_tags)
        for item in tr_tags:
            if item.find('td', class_="f3-1") is not None:
                date = str(item.find('td', class_="f3-1").text)
                date = date[0:10]
                date_datetime = datetime.datetime.strptime(date, "%d/%m/%Y")
                date = date_datetime.strftime("%Y-%m-%d")
               #  print(date)
            else:
                td_tags = item.find_all('td')
                if len(td_tags) > 1:


                    time = date + " " + str(td_tags[1].find_all('div')[1].text) + ":00"
                    HomeID = str(td_tags[2].find('a', href=True)['href'])
                    # print(HomeID)
                    try:
                        start = HomeID.index("(") + 1
                        end = HomeID.index(")", start)
                        HomeID = HomeID[start:end]
                    except ValueError:
                        HomeID = "0"
                    ft_score = str(td_tags[3].find('b').text)
                    ft_score = ft_score.replace('<font color="#FF0000">', '')
                    ft_score = ft_score.replace('</font>', '')
                    ft_score = ft_score.replace('&nbsp;', '')
                    ft_score = ft_score.replace(' ', '')
                    ft_hg = int(ft_score[0:ft_score.index('-')])
                    ft_ag = int(ft_score[ft_score.index('-') + 1: len(ft_score)])

                    AwayID = str(td_tags[4].find('a', href=True)['href'])
                    try:
                        start = AwayID.index("(") + 1
                        end = AwayID.index(")", start)
                        AwayID = AwayID[start:end]
                    except ValueError:
                        AwayID = "0"
                    neural = 'N' if td_tags[2].find('span') is not None else ""
                    ht_score = str(td_tags[5].find('font').text)
                    ht_score = ht_score.replace(' ', '')
                    ht_hg = -1
                    ht_ag = -1
                    separator = ''
                    if '-' in ht_score: separator = '-'
                    if ':' in ht_score: separator = ':'
                    if len(ht_score) > 2 and len(separator) == 1:
                        try:
                            ht_hg = int(ht_score[0: ht_score.index(separator)])
                        except: pass
                        try:
                            ht_ag = int(ht_score[ht_score.index(separator) + 1: len(ht_score)])
                        except: pass
                    handicap = ""
                    try:
                        handicap = str(td_tags[6].find('font').text)
                    except:
                        pass
                    if handicap == "":
                        try:
                            handicap = str(td_tags[6].find('b').text)
                        except:
                            pass
                    if handicap == "":
                        try:
                            handicap = str(td_tags[6].text)
                        except:
                            pass
                    state = ""
                    try:
                        state = str(td_tags[7].find('font').text)
                    except: pass
                    if str(td_tags[2].find('font').text) == "*":
                        handicap = "-" + handicap if handicap != "" else handicap
                    if len(td_tags[4].find_all('font')) > 1 and str(td_tags[4].find_all('font')[1].text) == "*":
                        if state == "Loss" or state == "Loss1/2":
                            state = state.replace('Loss', 'Win')
                        else:
                            state = state.replace('Win', 'Loss')
                    home_sup = ""
                    try:
                        home_sup = str(td_tags[2].find('sup').text)
                        home_sup = home_sup.replace('[', '')
                        home_sup = home_sup.replace(']', '')
                        home_sup = home_sup.replace(' ', '')
                    except: pass
                    away_sup = ""
                    try:
                        away_sup = str(td_tags[4].find('sup').text)
                        away_sup = away_sup.replace('[', '')
                        away_sup = away_sup.replace(']', '')
                        away_sup = away_sup.replace(' ', '')
                    except: pass
                    cursor_1 = connection.cursor()
                    sql = """SELECT Home FROM soccer_data WHERE Div_='{div}' AND Home="{home}" AND Away="{away}" AND Time_='{time}'
                    """.format(div=str(td_tags[0].text), time=time, home=str(td_tags[2].find('a').find('font').text),away=str(td_tags[4].find('a').find('font').text))
                    cursor_1.execute(sql)
                    item_rows = cursor_1.fetchall()
                    # print(sql)
                    if len(item_rows) >= 1 : 
                        # print("Exist!")
                        continue
                    sql = """INSERT INTO soccer_data(Div_,Time_,Home,FTHG,FTAG,Away,HomeID,AwayID,Neural,HTHG,HTAG,
                    HomeHandicap,Statement,HomeSup,AwaySup) VALUES ('{div}','{time}',"{home}",{ft_hg},{ft_ag},"{away}",
                    {home_id},{away_id},'{neural}',{ht_hg},{ht_ag},'{handicap}','{state}',
                                    '{home_sup}','{away_sup}')""".format(
                        div=str(td_tags[0].text), time=time, home=str(td_tags[2].find('a').find('font').text),
                        ft_hg=int(ft_hg), ft_ag=int(ft_ag), away=str(td_tags[4].find('a').find('font').text),
                        home_id=int(HomeID), away_id=int(AwayID), neural=neural, ht_hg=ht_hg, ht_ag=ht_ag,
                        handicap=handicap,state=state,home_sup=home_sup,away_sup=away_sup
                    )
                    # print(sql)
                    cursor_1 = connection.cursor()
                    cursor_1.execute(sql)
                    connection.commit()
                    sql = """INSERT INTO soccer_data(Div,Time_,Home,FTHG,FTAG,Away,HomeID,AwayID,Neural,HTHG,HTAG,
                    HomeHandicap
                                    ,Statement,HomeSup,AwaySup) VALUES ('{div}','{time}',"{home}",{ft_hg},{ft_ag},"{away}",
                                    {home_id},{away_id},'{neural}',{ht_hg},{ht_ag},'{handicap}','{state}',
                                    '{home_sup}','{away_sup}')""".format(
                        div=str(td_tags[0].text), time=time, home=str(td_tags[2].find('a').find('font').text),
                        ft_hg=int(ft_hg), ft_ag=int(ft_ag), away=str(td_tags[4].find('a').find('font').text),
                        home_id=int(HomeID), away_id=int(AwayID), neural=neural, ht_hg=ht_hg, ht_ag=ht_ag,
                        handicap=handicap,state=state,home_sup=home_sup,away_sup=away_sup
                    )
                    # print(sql)
                    cur = conn.cursor()
                    cur.execute(sql)
                    conn.commit()
        sql_delete_duplicate_rows = """delete   from soccer_data
        where    rowid not in
                (select  min(rowid)
                from    soccer_data
                group by Time_, Home, Away)"""
        cur.execute(sql_delete_duplicate_rows)
        conn.commit()
        print("Success!")
