import sqlite3
from sqlite3 import Error
import bs4

import kickscore as ks

from datetime import datetime
from dateutil.parser import parse

import numpy as np
import matplotlib.pyplot as plt
import itertools
import json

teams = set()
observations = list()
rows = []
items = []
cutoff = datetime(2020, 12, 1).timestamp()
conn = None
try:
    conn = sqlite3.connect('database.sqlite')
    cur = conn.cursor()
    cur.execute("SELECT Datetime, HomeTeam, AwayTeam, FTHG, FTAG FROM football_data order by Datetime ASC")
    # cur.execute("SELECT Datetime, HomeTeam, AwayTeam, FTHG, FTAG FROM football_data Where HomeTeam='Liverpool' or AwayTeam='Liverpool' order by Datetime ASC")
    rows = cur.fetchall()
    cur.execute("select HomeTeam from football_data UNION select AwayTeam from football_data")

    items_list = cur.fetchall()
    items = [i[0] for i in items_list]
    # items = ['Liverpool']
    # print(items)
except Error as e:
    print(e)

for row in rows:
    t_date = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S")
    if datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S") < parse('01/03/1970'):
        t = - (parse('01/03/1970') - datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S")).days * 86400 + 25200 + parse(
            '01/03/1970').timestamp()
    else:
        t = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S").timestamp()
    # t = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S").timestamp()
    if t > cutoff:
        break
    teams.add(row[1])
    teams.add(row[2])
    if int(row[3]) > int(row[4]):
        observations.append({
            "winners": [row[1]],
            "losers": [row[2]],
            "t": t,
        })
    else:
        observations.append({
            "winners": [row[2]],
            "losers": [row[1]],
            "t": t,
        })
# It is a bit more convenient to specify lengthscales in yearly units.
seconds_in_year = 365.25 * 24 * 60 * 60

model = ks.BinaryModel()
kernel = (ks.kernel.Constant(var=0.03)
          + ks.kernel.Matern32(var=0.138, lscale=1.753 * seconds_in_year))
for team in teams:
    model.add_item(team, kernel=kernel)

for obs in observations:
    model.observe(**obs)

converged = model.fit()
if converged:
    print("Model has converged.")

file = open("data_7m.txt", "r")
html = file.read()

matches = []
i = 1
while 0 < i:
    index_start_first_item = html.find("dtd2", i)
    separator = "dtd2"
    dtd1_index = html.find("dtd1", i)
    if index_start_first_item < 0 or (0 <= dtd1_index < index_start_first_item):
        index_start_first_item = dtd1_index
        separator = "dtd1"
    if index_start_first_item < 0: break
    index_end_first_item = html.find("</tr>", index_start_first_item + 6)
    item = html[index_start_first_item + 6: index_end_first_item]
    soup = bs4.BeautifulSoup(item, "html.parser")
    td_tags1 = soup.find_all('td')
    index_start_first_item = html.find(separator, index_end_first_item + 5)
    index_end_first_item = html.find("</tr>", index_start_first_item + 6)
    i = index_end_first_item + 6
    item = html[index_start_first_item + 6: index_end_first_item]
    soup = bs4.BeautifulSoup(item, "html.parser")
    td_tags2 = soup.find_all('td')

    matches.append({"Div": td_tags1[0].text, "Time": td_tags1[1].text, "HomeTeam": td_tags1[2].text,
                    "AvgH1": td_tags1[3].text, "AvgD1": td_tags1[4].text, "AvgA1": td_tags1[5].text,
                    "AwayTeam": td_tags1[6].text, "Result": td_tags1[7].find('b').text,
                    "AvgH2": td_tags2[0].text, "AvgD2": td_tags2[1].text,
                    "AvgA2": td_tags2[2].text, "predictH": 0, "select": 0})

for item in matches:
    avgh2 = 0.0
    try:
        avgh2 = float(item['AvgH1'])
    except:
        pass
    try:
        avgh2 = float(item['AvgH2'])
    except:
        pass

    if 2.2 <= avgh2 <= 2.7 or 1.6 <= avgh2 < 2:
        p_win = 0.0
        home = item["HomeTeam"].strip()
        away = item["AwayTeam"].strip()
        count_match = 0
        try:
            conn = sqlite3.connect('database.sqlite')
            cur = conn.cursor()
            cur.execute(
                "select count(*) from football_data where HomeTeam = '{home}' or AwayTeam = '{home}'".format(home=home))
            count_match = cur.fetchone()[0]
            if 300 <= count_match:
                cur.execute(
                    "select count(*) from football_data where HomeTeam = '{away}' or AwayTeam = '{away}'".format(
                        away=away))
                count_match = cur.fetchone()[0]

        except:
            pass

        # print(count_match)
        if 300 <= count_match:
            try:
                # print(item["HomeTeam"], " - ", item["AwayTeam"], " - Result: ", item["Result"])

                p_win, _ = model.probabilities([home], [away],
                                               t=datetime.now().timestamp())
                item["predictH"] = p_win
                if 2.2 <= avgh2 <= 2.7 and 0.4294376499106303 <= p_win <= 0.7548212743484494:
                    item["select"] = 1
                    print(home, " - ", away, ": Pick Home 0 P=", p_win, " - Result: ",
                          item["Result"])
                if 1.6 <= avgh2 < 1.7 and 0.6203990423734786 <= p_win <= 0.7677839457296286:
                    item["select"] = 1
                    print(home, " - ", away, ": Pick Home -0.75 P=", p_win, " - Result: ",
                          item["Result"])
                if 1.7 <= avgh2 < 2 and 0.6423996977595771 <= p_win <= 0.8240205818558227:
                    item["select"] = 1
                    print(home, " - ", away, ": Pick Home -0.5 P=", p_win, " - Result: ",
                          item["Result"])
                if 1.7 <= avgh2 < 2 and 0.23206961762242148 <= p_win <= 0.4616289290017005:
                    item["select"] = 1
                    p_away = 1 - p_win
                    print(home, " - ", away, ": Pick Away +0.5 P=", p_away, " - Result: ",
                          item["Result"])

            except:
                pass
# print(matches)
