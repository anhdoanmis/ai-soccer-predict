import sqlite3
from sqlite3 import Error
import datetime
import bs4
from selenium import webdriver
from selenium.webdriver.common.by import By
rows = []

conn = sqlite3.connect('data7m.db')
cur = conn.cursor()
date = datetime.datetime.now().strftime("%Y-%m-%d")
url = "http://live1.7msport.com/default_en.aspx?classid=&view=all&match=&line=no"

browser = webdriver.PhantomJS(executable_path=r'C:\phantomjs-2.1.1-windows\bin\phantomjs.exe')
browser.get(url)

try:
    element = browser.find_element(By.ID, "live_Table")
except:
    element = None
    print("Blank!")
    
if element != None:
    #print(element.get_attribute('innerHTML'))

    soup = bs4.BeautifulSoup(element.get_attribute('innerHTML'), "lxml")

    tr_tags = soup.find_all('tr')

    for i in range(4):
        del tr_tags[0]
    #print(tr_tags[2])
    for item in tr_tags:
        
        if item.find('td', class_="date") != None:
            date = str(item.find('td', class_="date").find('span').text)            
            date = date[0:10]
            try:            
                date_datetime = datetime.datetime.strptime(date, "%d/%m/%Y")
                date = date_datetime.strftime("%Y-%m-%d")
            except: break
            #  print(date)
        else:
            td_tags = item.find_all('td')
            if len(td_tags) > 6:
                
                #print(td_tags)
                time = date + " " + str(td_tags[2].text) + ":00"
                HomeID = str(td_tags[4].find('a', href=True)['href'])
                Home = str(td_tags[4].find('a', href=True).text)
                # print(HomeID)
                try:
                    start = HomeID.index("(") + 1
                    end = HomeID.index(")", start)
                    HomeID = HomeID[start:end]
                except ValueError:
                    HomeID = "0"
                Away = ""
                
                AwayID = str(td_tags[6].find('a', href=True)['href'])
                Away = str(td_tags[6].find('a', href=True).text)
                
                try:
                    start = AwayID.index("(") + 1
                    end = AwayID.index(")", start)
                    AwayID = AwayID[start:end]
                except ValueError:
                    AwayID = "0"
                    
                rows.append({'Time_': time, 'Home': Home, 'Away': Away}) 
    print("Success!")

count_win, count_all = 0.0, 0.0
count_goal_full_over_win, count_goal_full_over_all = 0.0, 0.0
count_goal_full_under_win, count_goal_full_under_all = 0.0, 0.0
count_goal_half_over_win, count_goal_half_over_all = 0.0, 0.0
count_goal_half_under_win, count_goal_half_under_all = 0.0, 0.0

number_of_check = 5

for row in rows:
    sql = """select Home,Away,FTHG, FTAG, HTHG, HTAG, Statement, P_HH
         from soccer_data where (Home="{team}" or Away="{team}") and Time_<'{time}' and Time_>'2018-01-01 00:00:00'
          order by Time_ DESC 
         """.format(team=row["Home"], time=row["Time_"])
    cur.execute(sql)
    home_rows = cur.fetchall()
    sql = """select Home,Away,FTHG, FTAG, HTHG, HTAG, Statement, P_HH
             from soccer_data where (Home="{team}" or Away="{team}") and Time_<'{time}' and Time_>'2018-01-01 00:00:00'
              order by Time_ DESC 
             """.format(team=row["Away"], time=row["Time_"])
    cur.execute(sql)
    away_rows = cur.fetchall()
    # Fulltime
    count_home_goal_win, count_home_goal_all, count_home_p, count_home_p_all = 0.0, 0.0, 0.0, 0.0
    for home_row in home_rows:
        if str(home_row[0]) == str(row["Home"]):
            count_home_goal_win += float(home_row[2]) 
            if home_row[7] != None:
                count_home_p += float(home_row[7]) 
                count_home_p_all += 1
        if str(home_row[1]) == str(row["Home"]):
            count_home_goal_win += float(home_row[3]) 
            if home_row[7] != None:
                count_home_p += 1 - float(home_row[7])
                count_home_p_all += 1
        count_home_goal_all += 1.0
        if count_home_goal_all >= number_of_check: break
    count_away_goal_win, count_away_goal_all, count_away_p, count_away_p_all = 0.0, 0.0, 0.0, 0.0
    for away_row in away_rows:
        if str(away_row[0]) == str(row["Away"]):
            count_away_goal_win += float(away_row[2]) 
            if away_row[7] != None:
                count_away_p += float(away_row[7]) 
                count_away_p_all += 1
        if str(away_row[1]) == str(row["Away"]):
            count_away_goal_win += float(away_row[3]) 
            if away_row[7] != None:
                count_away_p += 1 - float(away_row[7]) 
                count_away_p_all += 1
        count_away_goal_all += 1.0
        if count_away_goal_all >= number_of_check: break
    if count_home_goal_all >= number_of_check and count_away_goal_all >= number_of_check:
        # if (count_home_goal_win + count_away_goal_win) / (count_home_goal_all + count_away_goal_all) > 3.5:                                    
        #     #if float(row[4]) + float(row[5]) > 3.5:
        #     #    count_goal_full_over_win += 1.0
               
        #     count_goal_full_over_all += 1.0
        #     avg_p = (count_home_p + count_away_p)/(count_away_p_all + count_home_p_all) if count_away_p_all + count_home_p_all > 0 else -1.0   
        #     # print(row[1], ",", row[2], "-", row[3], ",", row[12], 
        #     # ", Over 3.5 FT:", count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all, "/", row[4], "+", row[5], "/", row[6], "+", row[7])
        #     print(row["Time_"], ",", "{:30s}".format(row["Home"]), "-", "{:30s}".format(row["Away"]), 
        #     ", Over 3.5 FT:", count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all)

        # if max((count_home_goal_win / count_home_goal_all) , (count_away_goal_win / count_away_goal_all)) < 0.8:
        if ((count_home_goal_win / count_home_goal_all) + (count_away_goal_win / count_away_goal_all)) < 1.8:
            print(row["Time_"], ",", "{:30s}".format(row["Home"]), "-", "{:30s}".format(row["Away"]), 
            ", Over 3.5 FT:", count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all)
        # if (count_home_goal_win / count_home_goal_all) + (count_away_goal_win / count_away_goal_all) < 1.5:  
        #     print(row["Time_"], ",", "{:30s}".format(row["Home"]), "-", "{:30s}".format(row["Away"]), 
        #     ", Under 3.5 FT:", count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all)
        if (count_home_goal_win + count_away_goal_win) / \
                                      (count_home_goal_all + count_away_goal_all) < 2.5:

            # if float(row[4]) + float(row[5]) < 2.5:
            #     count_goal_full_under_win += 1.0            
                # print(row[1], ",", row[2], ",", row[3], " Under 2.5 FT:", count_home_goal_win, "+", count_away_goal_win,
                #    "/", row[4], "+", row[5])
            count_goal_full_under_all += 1.0
        
    # Halftime
    # if float(row[6]) >= 0 and float(row[7]) >= 0:
    count_home_goal_win, count_home_goal_all, count_home_p, count_home_p_all = 0.0, 0.0, 0.0, 0.0
    count_home_goal_full_win = 0.0
    for home_row in home_rows:
        if float(home_row[4]) >= 0 and float(home_row[5]) >= 0:
            
            if str(home_row[0]) == str(row["Home"]):
                count_home_goal_win += float(home_row[4]) 
                count_home_goal_full_win += float(home_row[2])
                if home_row[7] != None:
                    count_home_p += float(home_row[7]) 
                    count_home_p_all += 1
                    
            if str(home_row[1]) == str(row["Home"]):
                count_home_goal_win += float(home_row[5])                  
                count_home_goal_full_win += float(home_row[3])
                if home_row[7] != None:
                    count_home_p += 1 - float(home_row[7]) 
                    count_home_p_all += 1 
            count_home_goal_all += 1.0
            if count_home_goal_all >= number_of_check: break
    count_away_goal_win, count_away_goal_all, count_away_p, count_away_p_all = 0.0, 0.0, 0.0, 0.0
    count_away_goal_full_win = 0.0
    for away_row in away_rows:
        if float(away_row[4]) >= 0 and float(away_row[5]) >= 0:
            if str(away_row[0]) == str(row["Away"]):
                count_away_goal_win += float(away_row[4]) 
                count_away_goal_full_win += float(away_row[2])                    
                if away_row[7] != None:
                    count_away_p += float(away_row[7]) 
                    count_away_p_all += 1
            if str(away_row[1]) == str(row["Away"]):
                count_away_goal_win += float(away_row[5]) 
                count_away_goal_full_win += float(away_row[3]) 
                if away_row[7] != None:
                    count_away_p += 1 - float(away_row[7]) 
                    count_away_p_all += 1
            count_away_goal_all += 1.0
            if count_away_goal_all >= number_of_check: break
    if count_home_goal_all >= number_of_check and count_away_goal_all >= number_of_check:
        # if (count_home_goal_win + count_away_goal_win) / \
        #                             (count_home_goal_all + count_away_goal_all) > 1.5:
        #     # if float(row[6]) + float(row[7]) > 1.5:
        #     #     count_goal_half_over_win += 1.0    
        #     # avg_p = (count_home_p + count_away_p)/(count_away_p_all + count_home_p_all) if count_away_p_all + count_home_p_all > 0 else -1.0                
        #     # print(row[1], ",", row[2], "-", row[3], ",", row[12],
        #     #      ", Over 1.5 HT: ", count_home_goal_full_win / count_home_goal_all, "+", count_away_goal_full_win / count_away_goal_all, "/",
        #     # count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all,
        #     #     "/", row[4], "+", row[5], "/", row[6], "+", row[7])
        #     print(row["Time_"], ",", "{:30s}".format(row["Home"]), "-", "{:30s}".format(row["Away"]), 
        #         ", Over 1.5 HT: ", count_home_goal_full_win / count_home_goal_all, "+", count_away_goal_full_win / count_away_goal_all, "/",
        # count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all)
        #     count_goal_half_over_all += 1.0 
        # if max((count_home_goal_win / count_home_goal_all) , (count_away_goal_win / count_away_goal_all)) <0.4 and \
        #        max((count_home_goal_full_win / count_home_goal_all) , (count_away_goal_full_win / count_away_goal_all)) <0.8 :   
        if ((count_home_goal_full_win / count_home_goal_all) + (count_away_goal_full_win / count_away_goal_all)) < 1:
            print(row["Time_"], ",", "{:30s}".format(row["Home"]), "-", "{:30s}".format(row["Away"]), 
                ", Over 1.5 HT: ", count_home_goal_full_win / count_home_goal_all, "+", count_away_goal_full_win / count_away_goal_all, "/",
        count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all)
        

        # if (count_home_goal_win / count_home_goal_all) + (count_away_goal_win / count_away_goal_all) < 0.5:
        #     # count_goal_half_under_win += 1.0            
        #     print(row["Time_"], ",", "{:30s}".format(row["Home"]), "-", "{:30s}".format(row["Away"]), 
        #         ", Under 1.5 HT: ", count_home_goal_full_win / count_home_goal_all, "+", count_away_goal_full_win / count_away_goal_all, "/",
        # count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all)
        if (count_home_goal_win + count_away_goal_win) / \
                                    (count_home_goal_all + count_away_goal_all) < 1:
            count_goal_half_under_all += 1
    # Home
    # number_of_check_2 = 5
    # count_home_win, count_home_all = 0.0, 0.0
    # for home_row in home_rows:
    #     if str(home_row[6]) == "Win" and str(home_row[0]) == str(row["Home"]):
    #         count_home_win += 1.0
    #     if str(home_row[6]) == "Win1/2" and str(home_row[0]) == str(row["Home"]):
    #         count_home_win += 0.5 
    #     if str(home_row[6]) == "Loss" and str(home_row[1]) == str(row["Home"]):
    #         count_home_win += 1.0 
    #     if str(home_row[6]) == "Loss1/2" and str(home_row[1]) == str(row["Home"]):
    #         count_home_win += 0.5 
    #     if str(home_row[6]) == "Win" or str(home_row[6]) == "Win1/2" or \
    #                              str(home_row[6]) == "Loss" or str(home_row[6]) == "Loss1/2":
    #         count_home_all += 1.0 
    #     if count_home_all >= number_of_check_2: break 
    
    # # Away
    # count_away_win, count_away_all = 0.0, 0.0
    # for away_row in away_rows:
    #     if str(away_row[6]) == "Win" and str(away_row[0]) == str(row["Away"]):
    #         count_away_win += 1.0 
    #     if str(away_row[6]) == "Win1/2" and str(away_row[0]) == str(row["Away"]):
    #         count_away_win += 0.5 
    #     if str(away_row[6]) == "Loss" and str(away_row[1]) == str(row["Away"]):
    #         count_away_win += 1.0 
    #     if str(away_row[6]) == "Loss1/2" and str(away_row[1]) == str(row["Away"]):
    #         count_away_win += 0.5 
    #     if str(away_row[6]) == "Win" or str(away_row[6]) == "Win1/2" or \
    #                              str(away_row[6]) == "Loss" or str(away_row[6]) == "Loss1/2":
    #         count_away_all += 1.0 
    #     if count_away_all >= number_of_check_2: break

    # rate_win, rate_lose = 0.8, 0.2
    # if count_home_all >= number_of_check_2 and count_away_all >= number_of_check_2: 
    #     if (count_home_win / count_home_all >= rate_win and count_away_win / count_away_all <= rate_lose):    
            
    #         print(row["Time_"], ",", "{:30s}".format(row["Home"]), "-", "{:30s}".format(row["Away"]), ",", " HW:", count_home_win / count_home_all, 
    #         " AW:", count_away_win / count_away_all)
# if count_all > 0:
#     print("H/A: ", count_win, "All: ", count_all, "Rate: ", count_win / count_all)
# else:
#     print("H/A: 0")
# if count_goal_half_over_all > 0:
#     print("Over 1.5 HT: ", count_goal_half_over_win, "All: ", count_goal_half_over_all, "Rate: ",
#         count_goal_half_over_win / count_goal_half_over_all)
# else:
#     print("Over 1.5 HT: 0")
# if count_goal_half_under_all > 0:
#     print("Under 1 HT: ", count_goal_half_under_win, "All: ", count_goal_half_under_all, "Rate: ",
#         count_goal_half_under_win / count_goal_half_under_all)
# else:
#     print("Under 1 HT: 0")
# if count_goal_full_over_all > 0:   
#     print("Over 3.5 FT: ", count_goal_full_over_win, "All: ", count_goal_full_over_all, "Rate: ",
#         count_goal_full_over_win / count_goal_full_over_all)
# else:
#     print("Over 3.5 FT: 0")
# if count_goal_full_under_all > 0:      
#     print("Under 2.5 FT: ", count_goal_full_under_win, "All: ", count_goal_full_under_all, "Rate: ",
#         count_goal_full_under_win / count_goal_full_under_all)
# else:
#     print("Under 2.5 FT: 0")

