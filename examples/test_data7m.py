import sqlite3
from sqlite3 import Error
import pymysql.cursors
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='',                             
                             db='db_7m',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
 
print ("connect successful!!")
rows = []

conn = sqlite3.connect('data7m.db')
cur = conn.cursor()
# # cur.execute("""select Div, Time_, Home, HomeID, Away, AwayID, FTHG, FTAG, HTHG, HTAG, Statement,HomeSup,AwaySup,HomeHandicap,Neural
# # from soccer_data""")
cur.execute("""select Div, by_team
from div""")
# # cur.execute("""select Div, Time_, Home, Away, FTHG, FTAG, HTHG, HTAG, Statement,HomeSup,AwaySup,P_HH,HomeHandicap
# # from soccer_data where Time_ > '2020-11-01 05:00:00' AND Time_ <= '2020-11-09 05:00:00'""")
rows = cur.fetchall()

count_win, count_all = 0.0, 0.0
count_goal_full_over_win, count_goal_full_over_all = 0.0, 0.0
count_goal_full_under_win, count_goal_full_under_all = 0.0, 0.0
count_goal_half_over_win, count_goal_half_over_all = 0.0, 0.0
count_goal_half_under_win, count_goal_half_under_all = 0.0, 0.0

number_of_check = 5
# cursor_1 = connection.cursor()
# cursor_1.execute("SELECT * FROM div__")
# rows = cursor_1.fetchall()

for row in rows:
    
    cursor_1 = connection.cursor()
    # sql = """INSERT INTO soccer_data(Div_, Time_, Home, HomeID, Away, AwayID, FTHG, FTAG, HTHG, HTAG, Statement,HomeSup,AwaySup,HomeHandicap,Neural) 
    #         VALUES("{Div}","{Time_}","{Home}",{HomeID},"{Away}",{AwayID},{FTHG},{FTAG},{HTHG},{HTAG},"{Statement}","{HomeSup}","{AwaySup}","{HomeHandicap}","{Neural}");
    #         """.format(Div=row[0],Time_=row[1],Home=row[2],HomeID=row[3],Away=row[4],AwayID=row[5],FTHG=row[6],FTAG=row[7],HTHG=row[8],HTAG=row[9],Statement=row[10],HomeSup=row[11],AwaySup=row[12],HomeHandicap=row[13],Neural=row[14])
    sql = """INSERT INTO div__(Div_,by_team) VALUES(%s,%s)"""
    # print(sql)
    # break
    cursor_1.execute(sql, (row[0],row[1]))
    connection.commit()
    # sql = """select Home,Away,FTHG, FTAG, HTHG, HTAG, Statement, P_HH
    #      from soccer_data where (Home="{team}" or Away="{team}") and Time_<'{time}' and Time_>'2018-01-01 00:00:00'
    #       order by Time_ DESC 
    #      """.format(team=row[2],
    #                                                                                                    time=row[1])
    # cur.execute(sql)
    # home_rows = cur.fetchall()
    # sql = """select Home,Away,FTHG, FTAG, HTHG, HTAG, Statement, P_HH
    #          from soccer_data where (Home="{team}" or Away="{team}") and Time_<'{time}' and Time_>'2018-01-01 00:00:00'
    #           order by Time_ DESC 
    #          """.format(team=row[3],
    #                                                                                                        time=row[1])
    # cur.execute(sql)
    # away_rows = cur.fetchall()
    # # Fulltime
    # count_home_goal_win, count_home_goal_all, count_home_p, count_home_p_all = 0.0, 0.0, 0.0, 0.0
    # ft_over = 0.0
    # ft_under = 0.0
    # for home_row in home_rows:
    #     if str(home_row[0]) == str(row[2]):
    #         count_home_goal_win += float(home_row[2]) 
    #         if home_row[7] != None:
    #             count_home_p += float(home_row[7]) 
    #             count_home_p_all += 1
    #     if str(home_row[1]) == str(row[2]):
    #         count_home_goal_win += float(home_row[3]) 
    #         if home_row[7] != None:
    #             count_home_p += 1 - float(home_row[7])
    #             count_home_p_all += 1
    #     count_home_goal_all += 1.0
    #     if count_home_goal_all >= number_of_check: break
    # count_away_goal_win, count_away_goal_all, count_away_p, count_away_p_all = 0.0, 0.0, 0.0, 0.0
    # for away_row in away_rows:
    #     if str(away_row[0]) == str(row[3]):
    #         count_away_goal_win += float(away_row[2]) 
    #         if away_row[7] != None:
    #             count_away_p += float(away_row[7]) 
    #             count_away_p_all += 1
    #     if str(away_row[1]) == str(row[3]):
    #         count_away_goal_win += float(away_row[3]) 
    #         if away_row[7] != None:
    #             count_away_p += 1 - float(away_row[7]) 
    #             count_away_p_all += 1
    #     count_away_goal_all += 1.0
    #     if count_away_goal_all >= number_of_check: break
    # if count_home_goal_all >= number_of_check and count_away_goal_all >= number_of_check:
    #     # if (count_home_goal_win + count_away_goal_win) / (count_home_goal_all + count_away_goal_all) > 3.5:                                    
    #     #     if float(row[4]) + float(row[5]) > 3.5:
    #     #         count_goal_full_over_win += 1.0
               
    #     #     count_goal_full_over_all += 1.0
    #     #     avg_p = (count_home_p + count_away_p)/(count_away_p_all + count_home_p_all) if count_away_p_all + count_home_p_all > 0 else -1.0   
    #     #     print("{:10s}".format(row[0]), ",", row[1], ",", "{:30s}".format(row[2]), "-", "{:30s}".format(row[3]), ",", "{:6s}".format(row[12]), 
    #     #     ", Over 3.5 FT:", count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all, "/", row[4], "+", row[5], "/", row[6], "+", row[7])
    #     #if abs((count_home_goal_win / count_home_goal_all) - (count_away_goal_win / count_away_goal_all)) > 2.4:
    #     #if abs((count_home_goal_win / count_home_goal_all) + (count_away_goal_win / count_away_goal_all)) < 1.5:# and \
    #        #min((count_home_goal_win / count_home_goal_all) , (count_away_goal_win / count_away_goal_all)) < 3.5:
    #     if ((count_home_goal_win / count_home_goal_all) + (count_away_goal_win / count_away_goal_all)) < 1.8:
       
    #         print("{:10s}".format(row[0]), ",", row[1], ",", "{:30s}".format(row[2]), "-", "{:30s}".format(row[3]), ",", "{:6s}".format(row[12]), 
    #          ", Over 4 FT:", count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all, "/", row[4], "+", row[5], "/", row[6], "+", row[7])  
    #         ft_over = 1.0
    #     if (count_home_goal_win / count_home_goal_all) + (count_away_goal_win / count_away_goal_all) <= 1.5:                                    
            
    #         #print("{:10s}".format(row[0]), ",", row[1], ",", "{:30s}".format(row[2]), "-", "{:30s}".format(row[3]), ",", "{:6s}".format(row[12]), 
    #         #", Under 1.5 FT:", count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all, "/", row[4], "+", row[5], "/", row[6], "+", row[7]) 
    #         ft_under = 1.0
    #     if (count_home_goal_win + count_away_goal_win) / \
    #                                   (count_home_goal_all + count_away_goal_all) < 2.5:

    #         if float(row[4]) + float(row[5]) < 2.5:
    #             count_goal_full_under_win += 1.0            
    #             # print(row[1], ",", row[2], ",", row[3], " Under 2.5 FT:", count_home_goal_win, "+", count_away_goal_win,
    #             #    "/", row[4], "+", row[5])
    #         count_goal_full_under_all += 1.0
    #     #if (count_home_goal_win / count_home_goal_all) < 2 or (count_away_goal_win / count_away_goal_all) < 2: 
        
    # #Halftime
    # if float(row[6]) >= 0 and float(row[7]) >= 0:
    #     count_home_goal_win, count_home_goal_all, count_home_p, count_home_p_all = 0.0, 0.0, 0.0, 0.0
    #     count_home_goal_full_win = 0.0
    #     for home_row in home_rows:
    #         if float(home_row[4]) >= 0 and float(home_row[5]) >= 0:
                
    #             if str(home_row[0]) == str(row[2]):
    #                 count_home_goal_win += float(home_row[4]) 
    #                 count_home_goal_full_win += float(home_row[2])
    #                 if home_row[7] != None:
    #                     count_home_p += float(home_row[7]) 
    #                     count_home_p_all += 1
                        
    #             if str(home_row[1]) == str(row[2]):
    #                 count_home_goal_win += float(home_row[5])                  
    #                 count_home_goal_full_win += float(home_row[3])
    #                 if home_row[7] != None:
    #                     count_home_p += 1 - float(home_row[7]) 
    #                     count_home_p_all += 1 
    #             count_home_goal_all += 1.0
    #             if count_home_goal_all >= number_of_check: break
    #     count_away_goal_win, count_away_goal_all, count_away_p, count_away_p_all = 0.0, 0.0, 0.0, 0.0
    #     count_away_goal_full_win = 0.0
    #     for away_row in away_rows:
    #         if float(away_row[4]) >= 0 and float(away_row[5]) >= 0:
    #             if str(away_row[0]) == str(row[3]):
    #                 count_away_goal_win += float(away_row[4]) 
    #                 count_away_goal_full_win += float(away_row[2])                    
    #                 if away_row[7] != None:
    #                     count_away_p += float(away_row[7]) 
    #                     count_away_p_all += 1
    #             if str(away_row[1]) == str(row[3]):
    #                 count_away_goal_win += float(away_row[5]) 
    #                 count_away_goal_full_win += float(away_row[3]) 
    #                 if away_row[7] != None:
    #                     count_away_p += 1 - float(away_row[7]) 
    #                     count_away_p_all += 1
    #             count_away_goal_all += 1.0
    #             if count_away_goal_all >= number_of_check: break
    #     if count_home_goal_all >= number_of_check and count_away_goal_all >= number_of_check:
    #         # if (count_home_goal_win + count_away_goal_win) / \
    #         #                             (count_home_goal_all + count_away_goal_all) > 1.5:
    #         #     if float(row[6]) + float(row[7]) > 1.5:
    #         #         count_goal_half_over_win += 1.0    
    #         #     avg_p = (count_home_p + count_away_p)/(count_away_p_all + count_home_p_all) if count_away_p_all + count_home_p_all > 0 else -1.0                
    #         #     print("{:10s}".format(row[0]), ",", row[1], ",", "{:30s}".format(row[2]), "-", "{:30s}".format(row[3]), ",", "{:6s}".format(row[12]),
    #         #          ", Over 1.5 HT: ", count_home_goal_full_win / count_home_goal_all, "+", count_away_goal_full_win / count_away_goal_all, "/",
    #         #     count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all,
    #         #         "/", row[4], "+", row[5], "/", row[6], "+", row[7])
    #         #     count_goal_half_over_all += 1.0 
    #         # if 1.3 <= abs((count_home_goal_win / count_home_goal_all) - (count_away_goal_win / count_away_goal_all)) and \
    #         #     min((count_home_goal_win / count_home_goal_all) , (count_away_goal_win / count_away_goal_all)) >= 0.7:   
    #         #if 1 >= abs((count_home_goal_win / count_home_goal_all) + (count_away_goal_win / count_away_goal_all)): # and \
    #         #if max((count_home_goal_win / count_home_goal_all) , (count_away_goal_win / count_away_goal_all)) <0.4 and \
    #         if ((count_home_goal_full_win / count_home_goal_all) + (count_away_goal_full_win / count_away_goal_all)) < 1:   
    #             #if ft_over > 0:
    #             print("{:10s}".format(row[0]), ",", row[1], ",", "{:30s}".format(row[2]), "-", "{:30s}".format(row[3]), ",", "{:6s}".format(row[12]),
    #                 ", Over 2 HT: ", count_home_goal_full_win / count_home_goal_all, "+", count_away_goal_full_win / count_away_goal_all, "/",
    #             count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all,
    #             "/", row[4], "+", row[5], "/", row[6], "+", row[7])
                 
    #             #count_goal_half_over_all += 1.0 
    #         # if (count_home_goal_win / count_home_goal_all) + (count_away_goal_win / count_away_goal_all) < 0.5:                              
    #         #     if ft_under > 0:
    #         #         print("{:10s}".format(row[0]), ",", row[1], ",", "{:30s}".format(row[2]), "-", "{:30s}".format(row[3]), ",", "{:6s}".format(row[12]),
    #         #          ", Under 0.5 HT: ", count_home_goal_full_win / count_home_goal_all, "+", count_away_goal_full_win / count_away_goal_all, "/",
    #         #         count_home_goal_win / count_home_goal_all, "+", count_away_goal_win / count_away_goal_all,
    #         #         "/", row[4], "+", row[5], "/", row[6], "+", row[7])
                 
    #         if (count_home_goal_win + count_away_goal_win) / \
    #                                     (count_home_goal_all + count_away_goal_all) < 1:

    #             if float(row[6]) + float(row[7]) < 1:
    #                 count_goal_half_under_win += 1.0            
    #                 # print(row[1], ",", row[2], ",", row[3], " Under 1.5 HT:", count_home_goal_win, "+", count_away_goal_win,
    #                 #    "/", row[6], "+", row[7])
    #             count_goal_half_under_all += 1
    # # Home
    # number_of_check_2 = 5
    # count_home_win, count_home_all = 0.0, 0.0
    # for home_row in home_rows:
    #     if str(home_row[6]) == "Win" and str(home_row[0]) == str(row[2]):
    #         count_home_win += 1.0
    #     if str(home_row[6]) == "Win1/2" and str(home_row[0]) == str(row[2]):
    #         count_home_win += 0.5 
    #     if str(home_row[6]) == "Loss" and str(home_row[1]) == str(row[2]):
    #         count_home_win += 1.0 
    #     if str(home_row[6]) == "Loss1/2" and str(home_row[1]) == str(row[2]):
    #         count_home_win += 0.5 
    #     if str(home_row[6]) == "Win" or str(home_row[6]) == "Win1/2" or \
    #                              str(home_row[6]) == "Loss" or str(home_row[6]) == "Loss1/2":
    #         count_home_all += 1.0 
    #     if count_home_all >= number_of_check_2: break 
    
    # # Away
    # count_away_win, count_away_all = 0.0, 0.0
    # for away_row in away_rows:
    #     if str(away_row[6]) == "Win" and str(away_row[0]) == str(row[3]):
    #         count_away_win += 1.0 
    #     if str(away_row[6]) == "Win1/2" and str(away_row[0]) == str(row[3]):
    #         count_away_win += 0.5 
    #     if str(away_row[6]) == "Loss" and str(away_row[1]) == str(row[3]):
    #         count_away_win += 1.0 
    #     if str(away_row[6]) == "Loss1/2" and str(away_row[1]) == str(row[3]):
    #         count_away_win += 0.5 
    #     if str(away_row[6]) == "Win" or str(away_row[6]) == "Win1/2" or \
    #                              str(away_row[6]) == "Loss" or str(away_row[6]) == "Loss1/2":
    #         count_away_all += 1.0 
    #     if count_away_all >= number_of_check_2: break

    # rate_win, rate_lose = 0.8, 0.4
    # if count_home_all >= number_of_check_2 and count_away_all >= number_of_check_2:
    #     if count_home_win / count_home_all >= rate_win and count_away_win / count_away_all <= rate_lose \
    #                   and str(row[8]) == "Win":
    #         count_win += 1
            

    #     if count_home_win / count_home_all >= rate_win and count_away_win / count_away_all <= rate_lose \
    #                     and str(row[8]) == "Win1/2":
    #         count_win += 0.5
            

    #     if count_home_win / count_home_all <= rate_lose and count_away_win / count_away_all >= rate_win \
    #                       and str(row[8]) == "Loss":
    #         count_win += 1
            
    #     if count_home_win / count_home_all <= rate_lose and count_away_win / count_away_all >= rate_win \
    #                         and str(row[8]) == "Loss1/2":
    #         count_win += 0.5
           
    #     if (count_home_win / count_home_all >= rate_win and count_away_win / count_away_all <= rate_lose) \
    #          and (str(row[8]) == "Loss1/2" or str(row[8]) == "Loss" or str(row[8]) == "Win1/2" or str(row[8]) == "Win"):    
    #         count_all += 1
    #         print("{:10s}".format(row[0]), ",", row[1], ",", "{:30s}".format(row[2]), "-", "{:30s}".format(row[3]), ",", "{:6s}".format(row[12]),
    #          ",", " HW:", count_home_win / count_home_all, " AW:", count_away_win / count_away_all, "/",
    #           row[4], "-", row[5], "/", row[6], "-", row[7])
        
# if count_all > 0:
#     print("H/A: ", count_win, "All: ", count_all, "Rate: ", count_win / count_all)
# else:
#     print("H/A: 0")
# if count_goal_half_over_all > 0:
#     print("Over 1.5 HT: ", count_goal_half_over_win, "All: ", count_goal_half_over_all, "Rate: ",
#         count_goal_half_over_win / count_goal_half_over_all)
# else:
#     print("Over 1.5 HT: 0")
# if count_goal_half_under_all > 0:
#     print("Under 1 HT: ", count_goal_half_under_win, "All: ", count_goal_half_under_all, "Rate: ",
#         count_goal_half_under_win / count_goal_half_under_all)
# else:
#     print("Under 1 HT: 0")
# if count_goal_full_over_all > 0:   
#     print("Over 3.5 FT: ", count_goal_full_over_win, "All: ", count_goal_full_over_all, "Rate: ",
#         count_goal_full_over_win / count_goal_full_over_all)
# else:
#     print("Over 3.5 FT: 0")
# if count_goal_full_under_all > 0:      
#     print("Under 2.5 FT: ", count_goal_full_under_win, "All: ", count_goal_full_under_all, "Rate: ",
#         count_goal_full_under_win / count_goal_full_under_all)
# else:
#     print("Under 2.5 FT: 0")

