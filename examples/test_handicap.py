import sqlite3
from sqlite3 import Error
def float_cal_handicap(HomeHandicap="0"):
    float_handicap = 0.0
    try:
        index_slash = HomeHandicap.find("/")
        if index_slash > -1:        
            float_handicap = float(HomeHandicap[0: index_slash]) + 0.25 if float(HomeHandicap[0: index_slash]) >= 0 else -0.25
        else:
            if HomeHandicap != "":
                float_handicap = float(HomeHandicap)    
    except: pass
    return float_handicap
def string_switch_statement(statement="Push"):
    if statement == "Win":
        return "Loss"
    if statement == "Win1/2":
        return "Loss1/2"
    if statement == "Loss":
        return "Win"
    if statement == "Loss1/2":
        return "Win1/2"
    if statement == "Push":
        return "Push"
rows = []

conn = sqlite3.connect('data7m.db')
cur = conn.cursor()
cur.execute("""select Div, Time_, Home, Away,HomeID,AwayID, FTHG, FTAG, HTHG, HTAG, Statement,HomeSup,AwaySup,P_HH,HomeHandicap
from soccer_data where Time_ > '2020-01-01 05:00:00' AND Time_ <= '2020-11-20 05:00:00' order by Time_ DESC""")
rows = cur.fetchall()
max_last_rows = 3
float_add_handicap = 1.25
for i in range(0, len(rows) - 2):
    if rows[i][14] != "":
        list_last_handicap_home = []
        list_last_handicap_away = []
        is_home_strong, is_home_weak, is_away_strong, is_away_weak = True, True, True, True
        for j in range(i + 1, len(rows)):
            if len(list_last_handicap_home) < max_last_rows:                                                     
                if rows[j][4] == rows[i][4]:
                    if rows[j][14] == "":
                        is_home_strong = False
                        is_home_weak = False
                    list_last_handicap_home.append(float_cal_handicap(rows[j][14]))
                    # print(rows[j])
                if rows[j][5] == rows[i][4]:
                    if rows[j][14] == "":
                        is_home_strong = False
                        is_home_weak = False
                    list_last_handicap_home.append(0 - float_cal_handicap(rows[j][14]))    
                    # print(rows[j])                
            if len(list_last_handicap_away) < max_last_rows:                               
                if rows[j][4] == rows[i][5]:
                    if rows[j][14] == "":
                        is_away_strong = False
                        is_away_weak = False   
                    list_last_handicap_away.append(float_cal_handicap(rows[j][14]))
                    # print(rows[j][14], " : ", float_cal_handicap(rows[j][14]))
                if rows[j][5] == rows[i][5]:
                    if rows[j][14] == "":
                        is_away_strong = False
                        is_away_weak = False 
                    list_last_handicap_away.append(0 - float_cal_handicap(rows[j][14]))
                    # print(rows[j][14], " : ", float_cal_handicap(rows[j][14]))
            if len(list_last_handicap_home) >= max_last_rows and len(list_last_handicap_away) >= max_last_rows:
                break
              
        if len(list_last_handicap_home) < max_last_rows:
            is_home_strong = False
            is_home_weak = False
        if len(list_last_handicap_away) < max_last_rows:
            is_away_strong = False
            is_away_weak = False 

        float_home_handicap = float_cal_handicap(rows[i][14]) 
        for j in range(0, max_last_rows):
            if is_home_strong == True and float_home_handicap + float_add_handicap > list_last_handicap_home[j]:          
                is_home_strong = False
            if is_home_weak == True and float_home_handicap - float_add_handicap < list_last_handicap_home[j]:              
                is_home_weak = False
            # if len(list_last_handicap_away)==0: print(rows[i])
            if is_away_strong == True and 0 - float_home_handicap + float_add_handicap > list_last_handicap_away[j]:
                is_away_strong = False
            if is_away_weak == True and 0 - float_home_handicap - float_add_handicap < list_last_handicap_away[j]:
                is_away_weak = False
        string_statement = ""
        if is_home_strong:
            string_statement += '+homestrong:' + rows[i][10]
        if is_home_weak:
            string_statement += '+homeweak:' + string_switch_statement(rows[i][10])
        if is_away_strong:
            string_statement += '+awaystrong:' + string_switch_statement(rows[i][10])
        if is_away_weak:           
            string_statement += '+awayweak:' + rows[i][10]
        # if is_home_strong and float_home_handicap < -1:
        if string_statement != "":
            sql = ''' UPDATE soccer_data
                                SET test_handicap = ?                               
                                WHERE Time_ = ?
                                AND HomeID = ?
                                AND AwayID = ?
                                '''
            cur.execute(sql, (string_statement , rows[i][1], rows[i][4], rows[i][5]))
            conn.commit()
            # print("home strong: ", is_home_strong, " home weak: ", is_home_weak, "away strong: ", is_away_strong, " away weak: ", is_away_weak)
            # print(rows[i])
            # print(float_home_handicap)
            # print(list_last_handicap_home)
            # print(list_last_handicap_away)
           


        