import plotly.graph_objects as go
import json
import sqlite3
from sqlite3 import Error

from datetime import datetime
items_list = []

try:
    conn = sqlite3.connect('database.sqlite')
    cur = conn.cursor()
    sql = """select Datetime, HomeTeam, AwayTeam, FTHG,FTAG, AvgH, AvgA, PredictH,
                CASE
	WHEN AvgH <= 2.7 AND AvgH >=2.3 THEN 0
	WHEN AvgH < 2.3 AND AvgH >= 2 THEN -0.25
	WHEN AvgH < 2 AND AvgH >= 1.7 THEN -0.5
	WHEN AvgH < 1.7 AND AvgH >= 1.6 THEN -0.75
	WHEN AvgH < 1.6 AND AvgH >= 1.5 THEN -1
	WHEN AvgH < 1.5 AND AvgH >= 1.4 THEN -1.25
	WHEN AvgH < 1.4 AND AvgH >= 1.3 THEN -1.5
	WHEN AvgH < 1.3 AND AvgH >= 1.2 THEN -1.75
	WHEN AvgH < 1.2 AND AvgH >= 1 THEN -2

	WHEN AvgA <= 2.7 AND AvgA >=2.3 THEN 0
	WHEN AvgA < 2.3 AND AvgA >= 2 THEN 0.25
	WHEN AvgA < 2 AND AvgA >= 1.7 THEN 0.5
	WHEN AvgA < 1.7 AND AvgA >= 1.6 THEN 0.75
	WHEN AvgA < 1.6 AND AvgA >= 1.5 THEN 1
	WHEN AvgA < 1.5 AND AvgA >= 1.4 THEN 1.25
	WHEN AvgA < 1.4 AND AvgA >= 1.3 THEN 1.5
	WHEN AvgA < 1.3 AND AvgA >= 1.2 THEN 1.75
	WHEN AvgA < 1.2 AND AvgA >= 1 THEN 2
	WHEN AvgH is NULL THEN NULL
	WHEN AvgA is NULL THEN NULL
	ELSE 0
	END AS HH,

CASE
	WHEN AvgH <= 2.7 AND AvgH >=2.3 AND FTHG > FTAG THEN 1
	WHEN AvgH < 2.3 AND AvgH >= 2 AND FTHG-0.25 > FTAG THEN 1
	WHEN AvgH < 2 AND AvgH >= 1.7 AND FTHG -0.5 > FTAG THEN 1
	WHEN AvgH < 1.7 AND AvgH >= 1.6 AND FTHG-0.75 > FTAG THEN 1
	WHEN AvgH < 1.6 AND AvgH >= 1.5 AND FTHG-1 > FTAG THEN 1
	WHEN AvgH < 1.5 AND AvgH >= 1.4 AND FTHG-1.25 > FTAG THEN 1
	WHEN AvgH < 1.4 AND AvgH >= 1.3 AND FTHG-1.5 > FTAG THEN 1
	WHEN AvgH < 1.3 AND AvgH >= 1.2 AND FTHG-1.75 > FTAG THEN 1
	WHEN AvgH < 1.2 AND AvgH >= 1 AND FTHG-2 > FTAG THEN 1

	WHEN AvgA <= 2.7 AND AvgA >=2.3 AND FTHG > FTAG THEN 1
	WHEN AvgA < 2.3 AND AvgA >= 2 AND FTHG+0.25 > FTAG THEN 1
	WHEN AvgA < 2 AND AvgA >= 1.7 AND FTHG+0.5 > FTAG THEN 1
	WHEN AvgA < 1.7 AND AvgA >= 1.6 AND FTHG+0.75 > FTAG THEN 1
	WHEN AvgA < 1.6 AND AvgA >= 1.5 AND FTHG+1 > FTAG THEN 1
	WHEN AvgA < 1.5 AND AvgA >= 1.4 AND FTHG+1.25 > FTAG THEN 1
	WHEN AvgA < 1.4 AND AvgA >= 1.3 AND FTHG+1.5 > FTAG THEN 1
	WHEN AvgA < 1.3 AND AvgA >= 1.2 AND FTHG+1.75 > FTAG THEN 1
	WHEN AvgA < 1.2 AND AvgA >= 1 AND FTHG+2 > FTAG THEN 1
	WHEN AvgH is NULL THEN NULL
	WHEN AvgA is NULL THEN NULL
	ELSE 0
	END AS HW, Div 
                FROM football_data where CAST(strftime('%Y',Datetime) AS INTEGER)>=2018"""
    cur.execute(sql)

    items_list = cur.fetchall()

    # items = ['Liverpool']
    # print(items)
except Error as e:
    print(e)
data_graph = []

x = []
y = []
x1 = []
y1 = []
for odd in [-2, -1.75, -1.5, -1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2]:
    list_predict = []
    for item in items_list:
        if item[5] is not None:
            """data_graph.append({'Datetime': item[0], 'HomeTeam': item[1], 'AwayTeam': item[2], 'FTHG': item[3],
                              'FTAG': item[4], 'AvgH': item[5], 'AvgA': item[6], 'PredictH': item[7], 'HH': item[8],
                               'HW': item[9],'Div': item[10]})"""
            if item[8] == odd:
                list_predict.append({'Datetime': item[0], 'HomeTeam': item[1], 'AwayTeam': item[2], 'FTHG': item[3],
                              'FTAG': item[4], 'AvgH': item[5], 'AvgA': item[6], 'PredictH': item[7], 'HH': item[8],
                               'HW': item[9],'Div': item[10]})
                """if item[9] == 1:
                    x1.append(item[7])
                    y1.append(item[9])
                else:
                    x.append(item[7])
                    y.append(item[9])"""
    for i in range(len(list_predict) - 1):
        for j in range(i + 1, len(list_predict)):
            if list_predict[i]['PredictH'] > list_predict[j]['PredictH']:
                t = list_predict[i]
                list_predict[i] = list_predict[j]
                list_predict[j] = t
    # print(list_predict)
    i = 0
    while i < len(list_predict) - 1:
    # for i in range(len(list_predict) - 1):
        count_win = list_predict[i]['HW']
        win_rate = float(count_win)
        predict_from = 0
        predict_to = 0
        i_add = 0
        print_count_match = 0
        print_count_win = 0
        # print(count_win)
        for j in range(i + 1, len(list_predict)):
            count_win += list_predict[j]['HW']
            # print(win_rate, " - ", float(count_win / (j - i - 1)))

            if j - i - 1 > 100 and win_rate >= 0.7 and (float(count_win / (j - i - 1)) < 0.7 or j == len(list_predict)):
                predict_from = list_predict[i]['PredictH']
                predict_to = list_predict[j]['PredictH']
                i_add = j + 1 if j < len(list_predict) else j
                print_count_match = j - i - 1
                print_count_win = count_win
                # print(win_rate)
            win_rate = float(count_win / (j - i - 1)) if j - i - 1 > 0 else win_rate
        i += 1
        if predict_from > 0 and predict_to > 0:
            i = i_add
            print('Home Handicap: ', odd, ' From: ', predict_from, ' To: ', predict_to, ' Match: ', print_count_match,
                  ' Win: ', print_count_win)

    i = 0
    while i < len(list_predict) - 1:
        # for i in range(len(list_predict) - 1):
        count_win = list_predict[i]['HW']
        win_rate = float(count_win)
        predict_from = 0
        predict_to = 0
        i_add = 0
        print_count_match = 0
        print_count_win = 0
        # print(count_win)
        for j in range(i + 1, len(list_predict)):
            count_win += list_predict[j]['HW']
            # print(win_rate, " - ", float(count_win / (j - i - 1)))

            if j - i - 1 > 100 and win_rate <= 0.3 and (float(count_win / (j - i - 1)) > 0.3 or j == len(list_predict)):
                predict_from = list_predict[i]['PredictH']
                predict_to = list_predict[j]['PredictH']
                i_add = j + 1 if j < len(list_predict) else j
                print_count_match = j - i - 1
                print_count_win = count_win
                # print(win_rate)
            win_rate = float(count_win / (j - i - 1)) if j - i - 1 > 0 else win_rate
        i += 1
        if predict_from > 0 and predict_to > 0:
            i = i_add
            print('Home Handicap (Pick Away): ', odd, ' From: ', predict_from, ' To: ', predict_to, ' Match: ',
                  print_count_match,
                  ' Win: ', print_count_win)
"""fig = go.Figure()
fig.add_trace(go.Scatter(
    x=x1,
    y=y1,
    marker=dict(color="green", size=1),
    mode="markers",
    name="Home Win",
))
fig.add_trace(go.Scatter(
    x=x,
    y=y,
    marker=dict(color="red", size=1),
    mode="markers",
    name="Home Lose",
))
fig.update_layout(title="Predict And Handicap",
                  xaxis_title="Predict",
                  yaxis_title="Home")

fig.show()"""




#with open('data_HDC_predict.json', 'w') as f:
#    json.dump(data_graph, f)