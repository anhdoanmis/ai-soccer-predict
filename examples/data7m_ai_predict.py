from datetime import datetime
from dateutil.parser import parse
from scipy.signal import find_peaks
import sqlite3
from sqlite3 import Error
import numpy as np
import matplotlib.pyplot as plt
import itertools
import json
import pyperclip
import kickscore as ks
import urllib.parse
import pymysql.cursors
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='',                             
                             db='db_7m',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
test_year = 2017
def training_data():
    # try:
    #     conn = sqlite3.connect('data7m.db')
    #     cur = conn.cursor()
    #     # cur.execute("SELECT Div FROM div WHERE home_strong is NULL AND away_strong is NULL")
    #     cur.execute("SELECT Div FROM div")
    #     rows_div = cur.fetchall()
    # except Error as e:
    #     print(e)
    cursor_1 = connection.cursor()
    cursor_1.execute("SELECT * FROM div__")
    rows_div = cursor_1.fetchall()
    for row_div in rows_div: 
        try:
            conn = sqlite3.connect('data7m.db')
            cur = conn.cursor()
            cur.execute("SELECT DISTINCT HomeID FROM soccer_data WHERE Div = '{Div}' ORDER BY HomeID".format(Div=row_div['Div_']))

            rows_team = cur.fetchall()
        except Error as e:
            print(e) 
        
        if len(rows_team) >= 10 and len(rows_team) < 300:
            in_rows_team = ''
            for row_team in rows_team:
                in_rows_team += str(row_team[0]) + ","
            in_rows_team = in_rows_team[0: len(in_rows_team) - 1]
            sql_in_rows_team = """SELECT ID,GP_1x2,GP_AH,GP_1x2_local_mm,GP_AH_local_mm,Name FROM team 
                Where ID IN (""" + in_rows_team + """)  
                AND GP_AH is not NULL and GP_AH <> '[]' AND GP_AH_local_mm <> '{\"peaks\": \"[]\", \"troughs\": \"[]\"}'
                order by ID"""
            
            
            try:
                conn = sqlite3.connect('data7m.db')
                cur = conn.cursor()
                cur.execute(sql_in_rows_team)

                rows_team_in_div = cur.fetchall()
            except Error as e:
                print(e)


            #string_strong_teams, string_weak_teams = "", ""

            sql_win_rate = """SELECT Home, HomeID, Away, AwayID, sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end) AS Win_count,
            sum(case when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 0.5 end) AS Loss_count,
            sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Count
            , sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Win_rate
            , sum(case when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Loss_rate
            FROM (SELECT * from soccer_data WHERE Div = '{Div}' AND CAST(strftime('%Y', Time_) AS INT) <= {test_year})       

            GROUP BY HomeID, AwayID
                    """.format(test_year=test_year, Div=row_div['Div_'])
                    
            try:
                conn = sqlite3.connect('data7m.db')
                cur = conn.cursor()
                cur.execute(sql_win_rate)
                rows_win_loss_rate = cur.fetchall()
            except Error as e:
                print(e) 
            
            max_distance_trough_peak, min_distance_trough_peak, float_array_distance_peak_trough = 0.0, 100.0, []
            float_array_home_low_win_rate, float_array_away_low_win_rate, float_array_home_high_win_rate, float_array_away_high_win_rate = [], [], [], []
            for row in rows_team_in_div:
                dict_GP_1x2, dict_GP_AH, dict_GP_1x2_local_mm, dict_GP_AH_local_mm = json.loads(row[1]), json.loads(row[2]), json.loads(row[3]), json.loads(row[4])
                dict_GP, dict_GP_mm = dict_GP_1x2, dict_GP_1x2_local_mm
                #dict_GP, dict_GP_mm = dict_GP_AH, dict_GP_AH_local_mm

                last_GP_index = len(dict_GP) - 1  
                list_peaks, list_troughs = json.loads(dict_GP_mm['peaks']), json.loads(dict_GP_mm['troughs'])
                if len(list_peaks) > 0:
                    if len(list_troughs) > 0:
                        if int(list_peaks[0]) > int(list_troughs[0]):
                            list_peaks.insert(0, 0)
                        else:
                            list_troughs.insert(0, 0)
                        if int(list_peaks[len(list_peaks) - 1]) > int(list_troughs[len(list_troughs) - 1]):
                            list_troughs.append(last_GP_index)
                        else:
                            list_peaks.append(last_GP_index)
                    else:
                        list_troughs.append(0)
                        list_troughs.append(last_GP_index)
                else:
                    list_peaks.append(0)
                    list_peaks.append(last_GP_index)
                
                float_distance_trough_peak = 0.0    
                for i in list_troughs:        
                    float_distance = 0.0
                    for j in list_peaks:
                        if j > i: 
                            float_distance = max(float(dict_GP[j]['Mean']) - float(dict_GP[i]['Mean']), float_distance)
                    float_distance_trough_peak = max(float_distance_trough_peak, float_distance)
                float_array_distance_peak_trough.append(float_distance_trough_peak)
            
                max_distance_trough_peak = max(max_distance_trough_peak, float_distance_trough_peak)
                min_distance_trough_peak = min(min_distance_trough_peak, float_distance_trough_peak)
                float_home_win_count, float_home_count = 0.0, 0.0
                float_away_win_count, float_away_count = 0.0, 0.0
                for row_win_loss_rate in rows_win_loss_rate:
                    if row_win_loss_rate[1] == row[0]:
                        float_home_win_count += 0 if row_win_loss_rate[4] is None else float(row_win_loss_rate[4])
                        float_home_count += 0 if row_win_loss_rate[6] is None else float(row_win_loss_rate[6])
                    if row_win_loss_rate[3] == row[0]:
                        float_away_win_count += 0 if row_win_loss_rate[5] is None else float(row_win_loss_rate[5])
                        float_away_count += 0 if row_win_loss_rate[6] is None else float(row_win_loss_rate[6])
                    
                if float_home_count > 0 and float_home_win_count/float_home_count < 0.5:
                    float_array_home_low_win_rate.append(row[0])
                if float_away_count > 0 and float_away_win_count/float_away_count < 0.5:
                    float_array_away_low_win_rate.append(row[0])
                if float_home_count > 0 and float_home_win_count/float_home_count > 0.8:
                    float_array_home_high_win_rate.append(row[0])
                if float_away_count > 0 and float_away_win_count/float_away_count > 0.8:
                    float_array_away_high_win_rate.append(row[0])
                # print(float_max_peaks)
                # print(float_min_troughs)

                # AH away strong year 2017
                # if 0.28 < float_distance_trough_peak:
                #     string_strong_teams += str(row[0]) + ","
                # if float_distance_trough_peak < 0.18:
                #     string_weak_teams += str(row[0]) + ","

                # away strong year 2017 from 1x2 away weak OK
                # if 0.35 < float_distance_trough_peak:
                #     string_strong_teams += str(row[0]) + ","
                # if float_distance_trough_peak < 0.2:
                #     string_weak_teams += str(row[0]) + ","

                # away strong year 2017 from 1x2 away strong
                # if 0.35 < float_distance_trough_peak:
                #     string_strong_teams += str(row[0]) + ","
                # if float_distance_trough_peak < 0.25:
                #     string_weak_teams += str(row[0]) + ","

                # 1x2 home strong year 2017
                # if 0.35 < float_distance_trough_peak:
                #     string_strong_teams += str(row[0]) + ","
                # if float_distance_trough_peak < 0.2:
                #     string_weak_teams += str(row[0]) + ","
                # if row[0] == 49:
                #     print(float_distance_trough_peak)
                #     break
            
            rows_team_in_div = [sub + (val, ) for sub, val in zip(rows_team_in_div, float_array_distance_peak_trough)]
            
            min_to_max_float_array_distance_peak_trough = sorted(float_array_distance_peak_trough, key=float)
            max_to_min_float_array_distance_peak_trough = list(reversed(min_to_max_float_array_distance_peak_trough))

            

            
            for row_team_in_div in rows_team_in_div:
                print(row_team_in_div[5], ' ', row_team_in_div[0],' ', row_team_in_div[6])
            # print(max_to_min_float_array_distance_peak_trough)
            # HOME_STRONG
            list_home_team, list_away_team, int_len_row = [], [], 0
            for i in range(len(max_to_min_float_array_distance_peak_trough) - 1):
                distance_home = max_to_min_float_array_distance_peak_trough[i]
                list_home, int_len_home = [], 0
                for row_team in rows_team_in_div:
                    if row_team[6] >= distance_home:
                        list_home.append(row_team[0])
                int_len_home = len(list_home)
                for float_home_low_win_rate in float_array_home_low_win_rate:   
                    if float_home_low_win_rate in list_home:                                      
                        list_home.remove(float_home_low_win_rate)
                    
                for j in range(i + 1, len(max_to_min_float_array_distance_peak_trough)):
                    distance_away = max_to_min_float_array_distance_peak_trough[j]
                    list_away, int_len_away = [], 0

                    for row_team in rows_team_in_div:
                        if row_team[6] <= distance_away:
                            list_away.append(row_team[0])
                    
                    int_len_away = len(list_away)
                    for float_away_high_win_rate in float_array_away_high_win_rate:   
                        if float_away_high_win_rate in list_away:                  
                            list_away.remove(float_away_high_win_rate)
                            

                    float_home_win_count, float_home_count = 0.0, 0.0                                
                    for row_win_loss_rate in rows_win_loss_rate:
                        if row_win_loss_rate[1] in list_home and row_win_loss_rate[3] in list_away:
                            float_home_win_count += 0 if row_win_loss_rate[4] is None else float(row_win_loss_rate[4])
                            float_home_count += 0 if row_win_loss_rate[6] is None else float(row_win_loss_rate[6])

                    if float_home_count > 0 and float(float_home_win_count/float_home_count) >= 0.65 and int_len_row < float_home_count \
                        and float(len(list_home))/float(int_len_home) > 0.65 and float(len(list_away))/float(int_len_away) > 0.65:
                        int_len_row = float_home_count
                        list_home_team = list_home
                        list_away_team = list_away
            sql_win_rate, string_home_team, string_away_team = '', '', ''
            if int_len_row > 0 and len(list_away_team) > 0 and len(list_away_team) > 0:
                for el_home_team in list_home_team:
                    string_home_team += str(el_home_team) + ',' 
                
                for el_away_team in list_away_team:
                    string_away_team += str(el_away_team) + ',' 
                sql_win_rate = """SELECT * from soccer_data WHERE Div = '{Div}' AND CAST(strftime('%Y', Time_) AS INT) <= {test_year} AND HomeID IN ({teams_home}) AND AwayID IN ({teams_away})""".format(teams_home=string_home_team[0: len(string_home_team) - 1], teams_away=string_away_team[0: len(string_away_team) - 1], test_year=test_year, Div=row_div[0])
                # pyperclip.copy(sql_win_rate)
                # print("Copied to Clipboard!")
                #break
            # else:
            #     print("Done!", row_div['Div_'])

            update_home_strong = json.dumps({"len_rows": int_len_row, "home": string_home_team, "away": string_away_team, "sql" : sql_win_rate})
            

            # AWAY_STRONG    
            list_home_team, list_away_team, int_len_row = [], [], 0
            for i in range(len(min_to_max_float_array_distance_peak_trough) - 1):
                distance_home = min_to_max_float_array_distance_peak_trough[i]
                list_home, int_len_home = [], 0
                for row_team in rows_team_in_div:
                    if row_team[6] <= distance_home:
                        list_home.append(row_team[0])
                int_len_home = len(list_home)
                for float_home_low_win_rate in float_array_home_low_win_rate:   
                    if float_home_low_win_rate in list_home:                                      
                        list_home.remove(float_home_low_win_rate)
                    
                for j in range(i + 1, len(min_to_max_float_array_distance_peak_trough)):
                    distance_away = min_to_max_float_array_distance_peak_trough[j]
                    list_away, int_len_away = [], 0

                    for row_team in rows_team_in_div:
                        if row_team[6] >= distance_away:
                            list_away.append(row_team[0])
                    
                    int_len_away = len(list_away)
                    for float_away_high_win_rate in float_array_away_high_win_rate:   
                        if float_away_high_win_rate in list_away:                  
                            list_away.remove(float_away_high_win_rate)

                    float_away_win_count, float_away_count = 0.0, 0.0                                
                    for row_win_loss_rate in rows_win_loss_rate:
                        if row_win_loss_rate[3] in list_away and row_win_loss_rate[1] in list_home:
                            float_away_win_count += 0 if row_win_loss_rate[5] is None else float(row_win_loss_rate[5])
                            float_away_count += 0 if row_win_loss_rate[6] is None else float(row_win_loss_rate[6])

                    if float_away_count > 0 and float(float_away_win_count/float_away_count) >= 0.65 and int_len_row < float_away_count \
                        and float(len(list_home))/float(int_len_home) > 0.65 and float(len(list_away))/float(int_len_away) > 0.65:
                        int_len_row = float_home_count
                        list_home_team = list_home
                        list_away_team = list_away
            sql_win_rate, string_home_team, string_away_team = '', '', ''
            if int_len_row > 0 and len(list_away_team) > 0 and len(list_away_team) > 0:
                for el_home_team in list_home_team:
                    string_home_team += str(el_home_team) + ',' 
                
                for el_away_team in list_away_team:
                    string_away_team += str(el_away_team) + ',' 
                sql_win_rate = """SELECT * from soccer_data WHERE Div = '{Div}' AND CAST(strftime('%Y', Time_) AS INT) <= {test_year} AND HomeID IN ({teams_home}) AND AwayID IN ({teams_away})""".format(teams_home=string_home_team[0: len(string_home_team) - 1], teams_away=string_away_team[0: len(string_away_team) - 1], test_year=test_year, Div=row_div[0])
                # pyperclip.copy(sql_win_rate)
                # print("Copied to Clipboard!")
                #break
            # else:
            #     print("Done!", row_div['Div_'])

            try:
                sql = ''' UPDATE div
                                SET home_strong = ?,
                                away_strong = ?                        
                                WHERE Div = ?
                                '''
                #print(graph_7m)
                cur.execute(sql, (update_home_strong,
                    json.dumps({"len_rows": int_len_row, "home": string_home_team, "away": string_away_team, "sql" : sql_win_rate}),
                    row_div['Div_'])
                )
                conn.commit()
                print("Updated ", row_div['Div_'])
            except:
                print("Error update ", row_div['Div_'])
                pass
        else:
            try:
                sql = ''' UPDATE div
                                SET home_strong = ?,
                                away_strong = ?                        
                                WHERE Div = ?
                                '''
                #print(graph_7m)
                cur.execute(sql, (json.dumps({"len_rows": 0, "home": '', "away": '', "sql" : ''}),
                    json.dumps({"len_rows": 0, "home": '', "away": '', "sql" : ''}),
                    row_div['Div_'])
                )
                conn.commit()
                print("Updated ", row_div['Div_'])
            except:
                print("Error update ", row_div['Div_'])
                pass
        # string_strong_teams = string_strong_teams[0: len(string_strong_teams) - 1]
        # string_weak_teams = string_weak_teams[0: len(string_weak_teams) - 1]
        
        # sql_home_strong_weak_per_year = """SELECT Home, HomeID, strftime('%Y', Time_), sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 end)  AS Win_count,
        # sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 when Statement= 'Loss' then 1 when Statement= 'Loss1/2' then 1 end) AS Count
        # , sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Win_rate
        # FROM soccer_data WHERE HomeID IN ({string_strong_teams}) 
        # AND AwayID IN ({string_weak_teams}) 
        # GROUP BY Home, HomeID, strftime('%Y', Time_)
        # HAVING sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) > 0.65
        # OR sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) < 0.35
        # ORDER BY HomeID, strftime('%Y', Time_),sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end)
        # """.format(string_strong_teams=string_strong_teams, string_weak_teams=string_weak_teams, test_year=test_year)
        # sql_home_strong = """SELECT Home, HomeID, sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 end) AS Win_count,
        # sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 when Statement= 'Loss' then 1 when Statement= 'Loss1/2' then 1 end) AS Count
        # , sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Win_rate
        # , Div
        # FROM (SELECT * from soccer_data WHERE CAST(strftime('%Y', Time_) AS INT) <= {test_year})
        # WHERE HomeID IN ({string_strong_teams}) 
        # AND AwayID IN ({string_weak_teams}) 
        # GROUP BY Div, HomeID
        # HAVING sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) > 0.65
        # AND sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 when Statement= 'Loss' then 1 when Statement= 'Loss1/2' then 1 end) > 10
        # ORDER BY sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end)
        # """.format(string_strong_teams=string_strong_teams, string_weak_teams=string_weak_teams, test_year=test_year)

        # sql_away_strong = """SELECT Home, HomeID, sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 end) AS Win_count,
        # sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 when Statement= 'Loss' then 1 when Statement= 'Loss1/2' then 1 end) AS Count
        # , sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Win_rate
        # , Div
        # FROM (SELECT * from soccer_data WHERE CAST(strftime('%Y', Time_) AS INT) <= {test_year})
        # WHERE HomeID IN ({string_weak_teams}) 
        # AND AwayID IN ({string_strong_teams}) 
        # GROUP BY Div, HomeID
        # HAVING sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) < 0.35
        # AND sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 when Statement= 'Loss' then 1 when Statement= 'Loss1/2' then 1 end) > 10
        # ORDER BY sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end)
        # """.format(string_strong_teams=string_strong_teams, string_weak_teams=string_weak_teams, test_year=test_year)

        # try:
        #     conn = sqlite3.connect('data7m.db')
        #     cur = conn.cursor()
        #     cur.execute(sql_away_strong)
        #     rows_7m = cur.fetchall()
        # except Error as e:
        #     print(e) 
        # string_strong_teams, string_div = '', ''

        # for row in rows_7m:
        #     string_strong_teams += str(row[1]) + ","
        #     string_div += '"' + str(row[5]) + '"' + ","
        # string_strong_teams = string_strong_teams[0: len(string_strong_teams) - 1]
        # string_div = string_div[0: len(string_div) - 1]

        # sql_home_strong_per_year = """SELECT strftime('%Y', Time_), sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 end) AS Win_count,
        # sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 when Statement= 'Loss' then 1 when Statement= 'Loss1/2' then 1 end) AS Count
        # , sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Win_rate
        # FROM (SELECT * from soccer_data WHERE CAST(strftime('%Y', Time_) AS INT) > {test_year})
        # WHERE HomeID IN ({string_strong_teams}) 
        # AND AwayID IN ({string_weak_teams}) 
        # AND Div IN ({string_div})
        # GROUP BY strftime('%Y', Time_)
        # ORDER BY strftime('%Y', Time_)
        # """.format(string_strong_teams=string_strong_teams, string_weak_teams=string_weak_teams, test_year=test_year, string_div= string_div)
        # sql_away_strong_per_year = """SELECT strftime('%Y', Time_) AS Year, sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 end) AS Win_count,
        # sum(case when Statement= 'Win' then 1 when Statement= 'Win1/2' then 1 when Statement= 'Loss' then 1 when Statement= 'Loss1/2' then 1 end) AS Count
        # , sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Win_rate
        # FROM (SELECT * from soccer_data WHERE CAST(strftime('%Y', Time_) AS INT) > {test_year})
        # WHERE HomeID IN ({string_weak_teams}) 
        # AND AwayID IN ({string_strong_teams}) 
        # AND Div IN ({string_div})
        # GROUP BY strftime('%Y', Time_)
        # ORDER BY strftime('%Y', Time_)
        # """.format(string_strong_teams=string_strong_teams, string_weak_teams=string_weak_teams, test_year=test_year, string_div= string_div)
        # pyperclip.copy(sql_away_strong_per_year)
        # print("Copied to Clipboard!")
        #print("SELECT * FROM soccer_data WHERE HomeID = 279 AND AwayID IN (", string_weak_teams, ")")

def testing_data():
    conn = sqlite3.connect('data7m.db')
    cur = conn.cursor()
    cur.execute("SELECT Div, home_strong, away_strong FROM div")
    rows_div = cur.fetchall()
    home_strong, away_home_strong, div_home_strong, away_strong, home_away_strong, div_away_strong = '', '', '', '', '', ''
    con_home_strong, con_away_strong = '', ''
    for row_div in rows_div:
        dic_home_strong = json.loads(row_div[1]) 
        dic_away_strong = json.loads(row_div[2])        
        
        if float(dic_home_strong['len_rows']) > 0:  
            div_home_strong = "'" + row_div[0] + "',"
            home_strong = str(dic_home_strong['home'])
            away_home_strong = str(dic_home_strong['away']) 
            con_home_strong += """(HomeID IN ({home_strong}) AND  AwayID IN ({away_home_strong}) AND Div = "{Div}")
                OR""".format(home_strong=home_strong[0: len(home_strong) - 1], away_home_strong = away_home_strong[0: len(away_home_strong) - 1], Div=row_div[0]) 
           
        
        if float(dic_away_strong['len_rows']) > 0:  
            div_away_strong += "'" + row_div[0] + "'," 
            away_strong += str(dic_away_strong['home'])
            home_away_strong += str(dic_away_strong['away'])

    # sql_home_strong = """SELECT * FROM soccer_data WHERE HomeID IN ({home_strong}) AND AwayID IN ({away_home_strong}) AND Div IN ({Div})
    #     """.format(home_strong=home_strong[0: len(home_strong) - 1], away_home_strong = away_home_strong[0: len(away_home_strong) - 1], Div=div_home_strong[0: len(div_home_strong) - 1])
    sql_home_strong = """SELECT Div, sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end) AS Win_count,
            sum(case when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 0.5 end) AS Loss_count,
            sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Count
            , sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Win_rate
            , sum(case when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 0.5 end)/sum(case when Statement= 'Win' then 1.0 when Statement= 'Win1/2' then 1.0 when Statement= 'Loss' then 1.0 when Statement= 'Loss1/2' then 1.0 end) AS Loss_rate 
			FROM (SELECT * FROM soccer_data WHERE CAST(strftime('%Y', Time_) AS INT) > {test_year} AND ({con_home_strong}))
            GROUP BY Div
        """.format(con_home_strong=con_home_strong[0: len(con_home_strong) - 2], test_year=test_year)
    pyperclip.copy(sql_home_strong)
    print("Copied to Clipboard!")


def print_plot_scores(model, teams_7m, resolution=None, timestamps=False):
    # colors = itertools.cycle(plt.cm.tab10(np.linspace(0, 1, 10)))
    if resolution is None:
        first = min(obs.t for obs in model.observations)
        last = max(obs.t for obs in model.observations)
        resolution = 100 / (last - first)
    # fig, ax = plt.subplots(figsize=figsize)
    
    # data_graph_index = 0
    
        # color = next(colors)
    div_data = []
    for team in teams_7m:
        ts, _, _ = model.item[team].scores
        data_graph = []
        ms_peaks = np.empty
        ms_troughs = np.empty
        if len(ts) > 1:
            first = min(ts)
            last = max(ts)
            ts = np.linspace(first, last, num=int(resolution * (last - first)))
            ms, vs = model.item[team].predict(ts)
            # std = np.sqrt(vs)
            if timestamps:
                ts = [datetime.datetime.fromtimestamp(t) for t in ts]
            # print(ts)
            # print(type(ts))
            # print(ms)
            # print(type(ms))
            ms_peaks, _ = find_peaks(ms) 
            ms_troughs, _= find_peaks(-ms)
            list_peaks, list_troughs = ms_peaks, ms_troughs
            last_GP_index = len(ms) - 1 
            if len(list_peaks) > 0:
                if len(list_troughs) > 0:
                    if int(list_peaks[0]) > int(list_troughs[0]):
                        list_peaks = np.insert(list_peaks, 0, 0)
                    else:
                        list_troughs = np.insert(list_troughs, 0, 0)
                    if int(list_peaks[len(list_peaks) - 1]) > int(list_troughs[len(list_troughs) - 1]):
                        list_troughs = np.append(list_troughs, last_GP_index)
                    else:
                        list_peaks = np.append(list_peaks, last_GP_index)
                else:
                    list_troughs = np.append(list_troughs, 0)
                    list_troughs = np.append(list_troughs, last_GP_index)
            else:
                list_peaks = np.append(list_peaks, 0)
                list_peaks = np.append(list_peaks, last_GP_index)
            float_distance_trough_peak = 0.0    
            for i in list_troughs:        
                float_distance = 0.0
                for j in list_peaks:
                    if j > i: 
                        float_distance = max(float(ms[j]) - float(ms[i]), float_distance)
                float_distance_trough_peak = max(float_distance_trough_peak, float_distance)
            
            for i in range(len(ts)):
                # print(ts[i])
                # print(type(ts[i]))
                data_graph.append({'Team': team, 'Date': str(ts[i]), 'Mean': ms[i]})
                # data_graph_index += 1
            div_data.append({'team': team, 'distance': float_distance_trough_peak, 'mean': data_graph})
            # except:
            #     print('Error ts ', team)        
            #     pass
    return div_data
    # ax.plot(ts, ms, color=color, label=name)
    # print(ax.plot(ts, ms, color=color, label=name))
    # ax.fill_between(ts, ms-std, ms+std, color=color, alpha=0.1)
    # for spine in ("top", "right", "bottom", "left"):
    # ax.spines[spine].set_visible(False)
    # ax.grid(axis="x", alpha=0.5)
    # ax.legend()



def build_div_mean(_test_year = 2017):
    # conn = sqlite3.connect('data7m.db')
    # cur = conn.cursor()
    # cur.execute("SELECT Div FROM div WHERE home_strong is NULL AND away_strong is NULL")
    # cur.execute("SELECT Div FROM div WHERE Div >'FOX U21'")
    # rows_div = cur.fetchall()
    cursor_1 = connection.cursor()
    cursor_1.execute("SELECT Div_ FROM div__  WHERE Div_ <> 'INTERF' AND Div_ <> 'INT CF'")
    rows_div = cursor_1.fetchall()

    # cutoff = datetime(2021, 12, 1).timestamp()
    for row_div in rows_div:
        teams_7m = set()        
        observations_7m = list()
        observations_7m_noh = list()
                
        sql = """SELECT Div_,Time_, Home, Away, FTHG, FTAG, HTHG, HTAG, Statement FROM soccer_data 
        WHERE Div_ = '{Div}' AND YEAR(Time_) >= {_test_year} order by Time_ ASC""".format(Div=row_div['Div_'], _test_year=_test_year)
        # cur = conn.cursor()
        # cur.execute(sql)
        # rows_match = cur.fetchall()
        cursor_1 = connection.cursor()
        cursor_1.execute(sql)
        rows_match = cursor_1.fetchall()
        for row_match in rows_match:
            t_date = datetime.datetime.strptime(row_match['Time_'], "%Y-%m-%d %H:%M:%S")
            if t_date < parse('01/03/1970'):
                t = - (parse('01/03/1970') - t_date).days * 86400 + 25200 + parse(
                    '01/03/1970').timestamp()
            else:
                t = t_date.timestamp()
            #t = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S").timestamp()
            
            if str(row_match['Home']) not in teams_7m: teams_7m.add(str(row_match['Home']))
            if str(row_match['Away']) not in teams_7m: teams_7m.add(str(row_match['Away']))
            #teams_7m_noh.add(row[9])
            #teams_7m_noh.add(row[10])
            if row_match['Statement'] == "Win" or row_match['Statement'] == "Win1/2":
                observations_7m.append({
                    "winners": [str(row_match['Home'])],
                    "losers": [str(row_match['Away'])],
                    "t": t,
                })
            if row_match['Statement'] == "Loss" or row_match['Statement'] == "Loss1/2":
                observations_7m.append({
                    "winners": [str(row_match['Away'])],
                    "losers": [str(row_match['Home'])],
                    "t": t,
                })
            if int(row_match['FTHG']) > int(row_match['FTAG']):
                observations_7m_noh.append({
                    "winners": [str(row_match['Home'])],
                    "losers": [str(row_match['Away'])],
                    "t": t,
                })
            if int(row_match['FTHG']) < int(row_match['FTAG']):
                observations_7m_noh.append({
                    "winners": [str(row_match['Away'])],
                    "losers": [str(row_match['Home'])],
                    "t": t,
                })
        seconds_in_year = 365.25 * 24 * 60 * 60

            # model = ks.BinaryModel()
        model_7m = ks.BinaryModel()
        model_7m_noh = ks.BinaryModel()
        kernel = (ks.kernel.Constant(var=0.03)
                + ks.kernel.Matern32(var=0.138, lscale=1.753*seconds_in_year))
        # for team in teams:
        #     model.add_item(team, kernel=kernel)

        # for obs in observations:
        #     model.observe(**obs)

        for team in teams_7m:
            model_7m.add_item(team, kernel=kernel)
            model_7m_noh.add_item(team, kernel=kernel)

        for obs in observations_7m:
            model_7m.observe(**obs)   

        for obs in observations_7m_noh:
            model_7m_noh.observe(**obs)


        converged_7m = model_7m.fit()         
           
        converged_7m_noh = model_7m_noh.fit()

        graph_7m = print_plot_scores(model_7m, teams_7m, resolution=10/seconds_in_year, timestamps=True)        
        graph_7m_noh =  print_plot_scores(model_7m_noh, teams_7m, resolution=10/seconds_in_year, timestamps=True)
        # for row in graph_7m_noh:
        #     print(row['team'], ' ', row['distance'])
        
        sql = ''' UPDATE div__
                        SET by_team = %s                                                    
                        WHERE Div_ = %s
                        '''
        #print(graph_7m)
        cursor_1 = connection.cursor()
        cursor_1.execute(sql, (urllib.parse.quote(json.dumps({"AH": graph_7m, "1x2": graph_7m_noh})),
            row_div['Div_'])
        )
        connection.commit()
        print("Updated ", row_div['Div_'])
        
def training_div_by_team(training_year = '2020'):
    # conn = sqlite3.connect('data7m.db')
    # cur = conn.cursor()
    # cur.execute("SELECT Div FROM div WHERE home_strong is NULL AND away_strong is NULL")
    # INT CF, INTERF
    # cur.execute("SELECT Div, by_team FROM div WHERE Div <> 'INTERF' AND Div <> 'INT CF'")
    # rows_div = cur.fetchall()SILC
    cursor_1 = connection.cursor()
    cursor_1.execute("SELECT Div_, by_team FROM div__ WHERE Div_ <> 'INTERF' AND Div_ <> 'INT CF'")
    rows_div = cursor_1.fetchall()

    con_home_strong = ''
    for row_div in rows_div:
        div_by_team = json.loads(urllib.parse.unquote(row_div['by_team']))
        AH_teams = []
        for team in div_by_team['AH']:
            AH_teams.append([team['team'], team['distance']])
        x2_teams = []
        for team in div_by_team['1x2']:
            x2_teams.append([team['team'], team['distance']])
        AH_teams = sorted(AH_teams, key=lambda x: x[1])
        x2_teams = sorted(x2_teams, key=lambda x: x[1])
        sql_win_rate = """SELECT divs.Home, divs.HomeID, divs.Away, divs.AwayID, 
        sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end) AS Win_count,
    sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end) AS Loss_count,
    sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Count
    , sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Win_rate
    , sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Loss_rate 
            FROM (SELECT * from soccer_data WHERE Div_ = "{Div}" AND YEAR(Time_) IN ({training_year})) divs       

            GROUP BY divs.HomeID, divs.AwayID
                    """.format(test_year=test_year, Div=row_div['Div_'], training_year=training_year)
        cursor_1 = connection.cursor()
        cursor_1.execute(sql_win_rate)
        rows_win_loss_rate = cursor_1.fetchall()
        float_array_home_low_win_rate, float_array_away_low_win_rate, float_array_home_high_win_rate, float_array_away_high_win_rate = [], [], [], []
        for row in AH_teams:                
            float_home_win_count, float_home_count = 0.0, 0.0
            float_away_win_count, float_away_count = 0.0, 0.0
            for row_win_loss_rate in rows_win_loss_rate:
                if row_win_loss_rate['Home'] == row[0]:
                    float_home_win_count += 0 if row_win_loss_rate['Win_count'] is None else float(row_win_loss_rate['Win_count'])
                    float_home_count += 0 if row_win_loss_rate['Count'] is None else float(row_win_loss_rate['Count'])
                if row_win_loss_rate['Away'] == row[0]:
                    float_away_win_count += 0 if row_win_loss_rate['Loss_count'] is None else float(row_win_loss_rate['Loss_count'])
                    float_away_count += 0 if row_win_loss_rate['Count'] is None else float(row_win_loss_rate['Count'])
                
            if float_home_count > 0 and float_home_win_count/float_home_count < 0.3:
                float_array_home_low_win_rate.append(row[0])
            if float_away_count > 0 and float_away_win_count/float_away_count < 0.3:
                float_array_away_low_win_rate.append(row[0])
            if float_home_count > 0 and float_home_win_count/float_home_count > 0.8:
                float_array_home_high_win_rate.append(row[0])
            if float_away_count > 0 and float_away_win_count/float_away_count > 0.8:
                float_array_away_high_win_rate.append(row[0])

        array_teams = x2_teams
        list_home_team, list_away_team, int_len_row = [], [], 0
        for i in range(len(array_teams) - 1, 0, -1):           
            
            list_home, int_len_home = [], 0
            for j in range(i, len(array_teams)):
                list_home.append(array_teams[j][0])
            int_len_home = len(list_home)
            for float_home_low_win_rate in float_array_home_low_win_rate:   
                if float_home_low_win_rate in list_home:                                      
                    list_home.remove(float_home_low_win_rate)
                
            for j in range(i - 1, -1, -1):
                list_away, int_len_away = [], 0
                
                for k in range(j, -1, -1):
                    list_away.append(array_teams[k][0])
                int_len_away = len(list_away)
                for float_away_high_win_rate in float_array_away_high_win_rate:   
                    if float_away_high_win_rate in list_away:                  
                        list_away.remove(float_away_high_win_rate)
                        

                float_home_win_count, float_home_count = 0.0, 0.0                                
                for row_win_loss_rate in rows_win_loss_rate:
                    if row_win_loss_rate['Home'] in list_home and row_win_loss_rate['Away'] in list_away:
                        float_home_win_count += 0 if row_win_loss_rate['Win_count'] is None else float(row_win_loss_rate['Win_count'])
                        float_home_count += 0 if row_win_loss_rate['Count'] is None else float(row_win_loss_rate['Count'])

                if int_len_row < float_home_count \
                    and float(float_home_win_count/float_home_count) >= 0.65:
                    # and float(len(list_home))/float(int_len_home) > 0.65 and float(len(list_away))/float(int_len_away) > 0.65:
                    int_len_row = float_home_count
                    list_home_team = list_home
                    list_away_team = list_away
        
        if int_len_row > 0 and len(list_home_team) > 0 and len(list_away_team) > 0:
            home_strong = ""
            for team in list_home_team:
                home_strong += "'" + team + "',"
            away_home_strong = ""
            for team in list_away_team:
                away_home_strong += "'" + team + "',"
            div_by_team['home_strong'] = home_strong[0: len(home_strong) - 1]
            div_by_team['away_home_strong'] = away_home_strong[0: len(away_home_strong) - 1]

        # away_strong
        list_home_team, list_away_team, int_len_row = [], [], 0
        for i in range(0, len(array_teams) - 1):           
            
            list_home, int_len_home = [], 0
            for j in range(0, i):
                list_home.append(array_teams[j][0])
            int_len_home = len(list_home)
            for float_home_high_win_rate in float_array_home_high_win_rate:   
                if float_home_high_win_rate in list_home:                                      
                    list_home.remove(float_home_high_win_rate)
                
            for j in range(i, len(array_teams)):
                list_away, int_len_away = [], 0
                
                for k in range(j, len(array_teams)):
                    list_away.append(array_teams[k][0])
                int_len_away = len(list_away)
                for float_away_low_win_rate in float_array_away_low_win_rate:   
                    if float_away_low_win_rate in list_away:                  
                        list_away.remove(float_away_low_win_rate)
                        

                float_away_win_count, float_away_count = 0.0, 0.0                                
                for row_win_loss_rate in rows_win_loss_rate:
                    if row_win_loss_rate['Home'] in list_home and row_win_loss_rate['Away'] in list_away:
                        float_away_win_count += 0 if row_win_loss_rate['Loss_count'] is None else float(row_win_loss_rate['Loss_count'])
                        float_away_count += 0 if row_win_loss_rate['Count'] is None else float(row_win_loss_rate['Count'])

                if int_len_row < float_away_count \
                    and float(float_away_win_count/float_away_count) >= 0.65:
                    # and float(len(list_home))/float(int_len_home) > 0.65 and float(len(list_away))/float(int_len_away) > 0.65:
                    int_len_row = float_away_count
                    list_home_team = list_home
                    list_away_team = list_away
        
        if int_len_row > 0 and len(list_home_team) > 0 and len(list_away_team) > 0:
            home_away_strong = ""
            for team in list_home_team:
                home_away_strong += "'" + team + "',"
            away_strong = ""
            for team in list_away_team:
                away_strong += "'" + team + "',"
            div_by_team['home_away_strong'] = home_away_strong[0: len(home_away_strong) - 1]
            div_by_team['away_strong'] = away_strong[0: len(away_strong) - 1]
        json_div_by_team = urllib.parse.quote(json.dumps(div_by_team))
        sql = ''' UPDATE div__
                        SET by_team = %s                                                    
                        WHERE Div_ = %s
                        '''
        #print(graph_7m)
        cursor_1 = connection.cursor()
        cursor_1.execute(sql, (json_div_by_team,
            row_div['Div_'])
        )
        connection.commit()

        print("Updated ", row_div['Div_'])
        
            
def test_div_by_team(training_year = '2018', _test_year = '2019', is_print_home = True):
    # conn = sqlite3.connect('data7m.db')
    # cur = conn.cursor()
    # cur.execute("SELECT Div FROM div WHERE home_strong is NULL AND away_strong is NULL")
    cursor_1 = connection.cursor()
    cursor_1.execute("SELECT Div_, by_team_{training_year}_65 AS by_team FROM div__".format(training_year=training_year))
    rows_div = cursor_1.fetchall()
    con_home_strong, con_away_strong = '', ''
    for row_div in rows_div:
        
        div_by_team = json.loads(urllib.parse.unquote(row_div['by_team']))
        
        if 'home_strong' in div_by_team and 'away_home_strong' in div_by_team:
            con_home_strong += """(Home IN ({home_strong}) AND  Away IN ({away_home_strong}) AND Div_ = "{Div}")
                OR""".format(home_strong=div_by_team['home_strong'], away_home_strong = div_by_team['away_home_strong'], Div=row_div['Div_']) 
        if 'home_away_strong' in div_by_team and 'away_strong' in div_by_team:
            con_away_strong += """(Home IN ({home_away_strong}) AND  Away IN ({away_strong}) AND Div_ = "{Div}")
                OR""".format(home_away_strong=div_by_team['home_away_strong'], away_strong = div_by_team['away_strong'], Div=row_div['Div_'])
    con_home_strong = con_home_strong.replace("','", '","').replace("('", '("').replace("')", '")')
    
    con_away_strong = con_away_strong.replace("','", '","').replace("('", '("').replace("')", '")')
   
    sql_home_strong = """SELECT divs.Div_, sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end) AS Win_count,
    sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end) AS Loss_count,
    sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Count
    , sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Win_rate
    , sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Loss_rate 
    FROM (SELECT * FROM soccer_data WHERE YEAR(Time_) IN ({_test_year}) AND ({con_home_strong})) AS divs
    WHERE (divs.Div_ LIKE '% D1' OR divs.Div_ LIKE '% D2' OR divs.Div_ LIKE '% D3' OR divs.Div_ LIKE '% D4' OR divs.Div_ LIKE '% D5' OR divs.Div_ LIKE '% CUP' OR divs.Div_ LIKE '% SC' OR divs.Div_ LIKE '% PR'
	 ) AND (divs.Div_ LIKE 'ALB %' OR divs.Div_ LIKE 'ALG %' OR divs.Div_ LIKE 'BEL %' OR
	 divs.Div_ LIKE 'CHI %' OR divs.Div_ LIKE 'ENG %' OR divs.Div_ LIKE 'DEN %' OR 
	  divs.Div_ LIKE 'FRA %' OR divs.Div_ LIKE 'GEO %' OR divs.Div_ LIKE 'GRE %' OR divs.Div_ LIKE 'HUN %' OR divs.Div_ LIKE 'POL %' OR divs.Div_ LIKE 'POR %'
	  OR divs.Div_ LIKE 'SER %' OR divs.Div_ LIKE 'SPA %' OR divs.Div_ LIKE 'POR %' OR divs.Div_ LIKE 'VEN %'
	  )
    GROUP BY divs.Div_


        """.format(con_home_strong=con_home_strong[0: len(con_home_strong) - 2], _test_year=_test_year)
    sql_away_strong = """SELECT divs.Div_, sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end) AS Win_count,
    sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end) AS Loss_count,
    sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Count
    , sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Win_rate
    , sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Loss_rate 
    FROM (SELECT * FROM soccer_data WHERE YEAR(Time_) IN ({_test_year}) AND ({con_away_strong})) AS divs
    WHERE (divs.Div_ LIKE '% D1' OR divs.Div_ LIKE '% D2' OR divs.Div_ LIKE '% D3' OR divs.Div_ LIKE '% D4' OR divs.Div_ LIKE '% D5' OR divs.Div_ LIKE '% CUP' OR divs.Div_ LIKE '% SC' OR divs.Div_ LIKE '% PR'
	 ) AND (divs.Div_ LIKE 'ARG %' OR divs.Div_ LIKE 'BEL %' OR divs.Div_ LIKE 'BLR %' OR divs.Div_ LIKE 'BRA %' OR divs.Div_ LIKE 'CHA %' OR divs.Div_ LIKE 'CHI %'
	 OR divs.Div_ LIKE 'COL %' OR divs.Div_ LIKE 'CZE %' OR divs.Div_ LIKE 'ENG %' OR divs.Div_ LIKE 'FIN %' OR divs.Div_ LIKE 'FRA %' OR divs.Div_ LIKE 'GER %'
	  OR divs.Div_ LIKE 'GUA %' OR divs.Div_ LIKE 'HUN %' OR divs.Div_ LIKE 'JPN %'  OR divs.Div_ LIKE 'POR %')
    GROUP BY divs.Div_
        """.format(con_away_strong=con_away_strong[0: len(con_away_strong) - 2], _test_year=_test_year)
    if is_print_home: pyperclip.copy(sql_home_strong) 
    else: pyperclip.copy(sql_away_strong)
    print("Copied to Clipboard!")        
import datetime
import bs4
from selenium import webdriver
from selenium.webdriver.common.by import By
def predict_div_by_team():
    url = "http://live1.7msport.com/default_en.aspx?classid=&view=all&match=&line=no"

    browser = webdriver.PhantomJS(executable_path=r'C:\phantomjs-2.1.1-windows\bin\phantomjs.exe')
    browser.get(url)
    rows = []
    try:
        element = browser.find_element(By.ID, "live_Table")
    except:
        element = None
        print("Blank!")
        
    if element != None:
        #print(element.get_attribute('innerHTML'))

        soup = bs4.BeautifulSoup(element.get_attribute('innerHTML'), "lxml")

        tr_tags = soup.find_all('tr')

        for i in range(4):
            del tr_tags[0]
        #print(tr_tags[2])
        for item in tr_tags:
            
            if item.find('td', class_="date") != None:
                date = str(item.find('td', class_="date").find('span').text)            
                date = date[0:10]
                try:            
                    date_datetime = datetime.datetime.strptime(date, "%d/%m/%Y")
                    date = date_datetime.strftime("%Y-%m-%d")
                except: break
                #  print(date)
            else:
                td_tags = item.find_all('td')
                if len(td_tags) > 6:
                    
                    #print(td_tags)
                    div = str(td_tags[1].text)
                    time = date + " " + str(td_tags[2].text) + ":00"
                    HomeID = str(td_tags[4].find('a', href=True)['href'])
                    Home = str(td_tags[4].find('a', href=True).text)
                    # print(HomeID)
                    try:
                        start = HomeID.index("(") + 1
                        end = HomeID.index(")", start)
                        HomeID = HomeID[start:end]
                    except ValueError:
                        HomeID = "0"
                    Away = ""
                    
                    AwayID = str(td_tags[6].find('a', href=True)['href'])
                    Away = str(td_tags[6].find('a', href=True).text)
                    
                    try:
                        start = AwayID.index("(") + 1
                        end = AwayID.index(")", start)
                        AwayID = AwayID[start:end]
                    except ValueError:
                        AwayID = "0"
                        
                    rows.append({'Div': div,'Time_': time, 'Home': Home, 'Away': Away}) 
        print("Success!")
        # print(rows)
        if len(rows) > 0:
            cursor_1 = connection.cursor()
            cursor_1.execute("SELECT Div_, by_team_2020_75 FROM div__")
            rows_div = cursor_1.fetchall()
            con_home_strong, con_away_strong = '', ''
            for row_div in rows_div:
                
                div_by_team = json.loads(urllib.parse.unquote(row_div['by_team_2020_75']))
                
                if 'home_strong' in div_by_team and 'away_home_strong' in div_by_team:
                    con_home_strong += """(Home IN ({home_strong}) AND  Away IN ({away_home_strong}) AND Div_ = "{Div}")
                        OR""".format(home_strong=div_by_team['home_strong'], away_home_strong = div_by_team['away_home_strong'], Div=row_div['Div_']) 
                    for row in rows:
                        if row['Div'] == row_div['Div_'] and \
    row['Div'] in ['ENG PR','ENG D1','ENG D2','GER D1','GER D2','GERC','ITA Cup','ITA D1','ITA D2','RUS CUP','RUS D1','RUS D2','SPA CUP','SPA D1','SPA D2'] and \
                        div_by_team['home_strong'].find('"'+ row['Home'] + '"') >= 0 and div_by_team['away_home_strong'].find('"'+ row['Away'] + '"') >= 0 :
                            print(row, " Pick Home")
                        if row['Div'] == row_div['Div_'] and \
    row['Div'] in ['ARG D1','ARG D2','ARG Cup','BEL Cup','BEL D1','BEL D2','BRA CUP','BRA D1','BRA D2'] and \
                        div_by_team['home_strong'].find('"'+ row['Home'] + '"') >= 0 and div_by_team['away_home_strong'].find('"'+ row['Away'] + '"') >= 0 :
                            print(row, "Pick Away")      
                if 'home_away_strong' in div_by_team and 'away_strong' in div_by_team:
                    con_away_strong += """(Home IN ({home_away_strong}) AND  Away IN ({away_strong}) AND Div_ = "{Div}")
                        OR""".format(home_away_strong=div_by_team['home_away_strong'], away_strong = div_by_team['away_strong'], Div=row_div['Div_'])
                    for row in rows:
                        if row['Div'] == row_div['Div_'] and \
    row['Div'] in ['ENG PR','ENG D1','ENG D2','GER D1','GER D2','GERC','ITA Cup','ITA D1','ITA D2','SPA CUP','SPA D1','SPA D2'] and \
                        div_by_team['home_away_strong'].find('"'+ row['Home'] + '"') >= 0 and div_by_team['away_strong'].find('"'+ row['Away'] + '"') >= 0 :
                            print(row, " Pick Home")
            con_home_strong = con_home_strong.replace("','", '","').replace("('", '("').replace("')", '")')
            
            con_away_strong = con_away_strong.replace("','", '","').replace("('", '("').replace("')", '")')


        
            sql_home_strong_pick_home = """SELECT divs.Div_, sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end) AS Win_count,
            sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end) AS Loss_count,
            sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Count
            , sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Win_rate
            , sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Loss_rate 
            FROM (SELECT * FROM soccer_data WHERE YEAR(Time_) IN (2021) AND ({con_home_strong})) AS divs
            WHERE divs.Div_ IN ('ENG PR','ENG D1','ENG D2','GER D1','GER D2','GERC','ITA Cup','ITA D1','ITA D2','RUS CUP','RUS D1','RUS D2','SPA CUP','SPA D1','SPA D2')
            GROUP BY divs.Div_""".format(con_home_strong=con_home_strong[0: len(con_home_strong) - 2], test_year=test_year)

            sql_home_strong_pick_away = """SELECT divs.Div_, sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end) AS Win_count,
            sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end) AS Loss_count,
            sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Count
            , sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Win_rate
            , sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Loss_rate 
            FROM (SELECT * FROM soccer_data WHERE YEAR(Time_) IN (2021) AND ({con_home_strong})) AS divs
            WHERE divs.Div_ IN ('ARG D1','ARG D2','ARG Cup','BEL Cup','BEL D1','BEL D2','BRA CUP','BRA D1','BRA D2')
            GROUP BY divs.Div_""".format(con_home_strong=con_home_strong[0: len(con_home_strong) - 2], test_year=test_year)
            
            sql_away_strong_pick_home = """SELECT divs.Div_, sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end) AS Win_count,
            sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end) AS Loss_count,
            sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Count
            , sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Win_rate
            , sum(case when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 0.5 end)/sum(case when divs.Statement= 'Win' then 1.0 when divs.Statement= 'Win1/2' then 1.0 when divs.Statement= 'Loss' then 1.0 when divs.Statement= 'Loss1/2' then 1.0 end) AS Loss_rate 
            FROM (SELECT * FROM soccer_data WHERE YEAR(Time_) IN (2021) AND ({con_away_strong})) AS divs
            WHERE divs.Div_ IN ('SPA CUP','SPA D1','SPA D2')
            GROUP BY divs.Div_
                """.format(con_away_strong=con_away_strong[0: len(con_away_strong) - 2], test_year=test_year)
            
# testing_data()
# build_div_mean(_test_year = 2018)
# training_div_by_team(training_year = '2019')
test_div_by_team(training_year = '2018_2019', _test_year = '2019', is_print_home = True)
# predict_div_by_team()
